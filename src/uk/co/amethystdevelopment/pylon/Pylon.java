package uk.co.amethystdevelopment.pylon;

import java.io.IOException;
import java.sql.SQLException;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import uk.co.amethystdevelopment.pylon.commands.Pylon_CommandRegistry;
import uk.co.amethystdevelopment.pylon.listeners.Pylon_BlockListener;
import uk.co.amethystdevelopment.pylon.listeners.Pylon_CamOverlordListener;
import uk.co.amethystdevelopment.pylon.listeners.Pylon_JumpListener;
import uk.co.amethystdevelopment.pylon.listeners.Pylon_PlayerListener;
import uk.co.amethystdevelopment.pylon.listeners.Pylon_TelnetListener;
import uk.co.amethystdevelopment.pylon.listeners.Pylon_ToggleableEventsListener;
import uk.co.amethystdevelopment.pylon.listeners.Pylon_WorldEditListener;
import uk.co.amethystdevelopment.pylon.worlds.Pylon_WorldManager;

public class Pylon extends JavaPlugin
{

    public static Pylon plugin;
    public static Pylon_CommandRegistry commandregistry;
    public static Pylon_PlayerListener playerlistener;
    public static Pylon_TelnetListener telnetlistener;
    public static Pylon_ToggleableEventsListener toggleableeventslistener;
    public static Pylon_CamOverlordListener camzielistener;
    public static Pylon_BlockListener blocklistener;
    public static Pylon_JumpListener jumplistener;
    public static Pylon_WorldEditListener worldeditlistener;
    public static Pylon_SocketServer socketServer;
    public static FileConfiguration config;
    public static Thread thread;

    public static boolean screwover = false;

    @Override
    public void onEnable()
    {
        plugin = this;
        PluginDescriptionFile pdf = this.getDescription();
        getLogger().log(Level.INFO, "{0}{1} v. {2} by {3} has been enabled!", new Object[]
        {
            ChatColor.BLUE, pdf.getName(), pdf.getVersion(), pdf.getAuthors()
        });
        config = this.getConfig();
        this.saveDefaultConfig();
        try
        {
            Pylon_DatabaseInterface.prepareDatabase();
        }
        catch(Exception ex)
        {
            plugin.handleException(ex);
        }
        commandregistry = new Pylon_CommandRegistry();
        playerlistener = new Pylon_PlayerListener();
        telnetlistener = new Pylon_TelnetListener();
        toggleableeventslistener = new Pylon_ToggleableEventsListener();
        camzielistener = new Pylon_CamOverlordListener();
        blocklistener = new Pylon_BlockListener();
        jumplistener = new Pylon_JumpListener();
        worldeditlistener = new Pylon_WorldEditListener();
        if(!config.getBoolean("general.owner"))
        {
            System.out.println("Welcome to the Pylon Plugin, an Owner has not yet been defined, to set yourself to Owner please run \"/owner " + Pylon_Commons.verifyCode + "\" in-game to set yourself to the Owner rank!");
        }
        else
        {
            Pylon_Commons.verifyCode = null;
        }
        Pylon_WorldManager.loadWorldsFromConfig();
        Pylon_Announcements.setup();
        for(Player player : Bukkit.getOnlinePlayers())
        {
            try
            {
                if(Pylon_DatabaseInterface.getBooleanFromTable("UUID", player.getUniqueId().toString(), "IMPOSTER", "PLAYERS"))
                {
                    Pylon_Commons.imposters.add(player.getName());
                }
            }
            catch(SQLException ex)
            {
                Logger.getLogger(Pylon.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        Pylon_RestManager.sendMessage(config.getInt("rest.statusid"), "The Pylon Plugin has been enabled.");
        this.getServer().getServicesManager().register(Function.class, Pylon_Rank.ADMIN_SERVICE, plugin, ServicePriority.Highest);
        socketServer = new Pylon_SocketServer();
        thread = new Thread(socketServer);
        thread.start();
        new BukkitRunnable()
        {

            @Override
            public void run()
            {
                Pylon_Rank.nicks.clear();
                Pylon_Rank.tags.clear();
                Pylon_Rank.ranks.clear();
            }
        }.runTaskTimerAsynchronously(plugin, 0L, 20L * 300L);
    }

    @Override
    public void onDisable()
    {
        getLogger().log(Level.INFO, "{0}Unloading all AFM Worlds", ChatColor.RED);
        Pylon_WorldManager.unloadWorlds();
        getLogger().log(Level.INFO, "{0}Unloading all AFM Listeners", ChatColor.RED);
        HandlerList.unregisterAll(plugin);
        getLogger().log(Level.INFO, "{0}Unloading all AFM Commands", ChatColor.RED);
        Pylon_CommandRegistry.unregisterCommands();
        PluginDescriptionFile pdf = this.getDescription();
        getLogger().log(Level.INFO, "{0}{1} has been disabled!", new Object[]
        {
            ChatColor.RED, pdf.getName()
        });
        Pylon_RestManager.sendMessage(config.getInt("rest.statusid"), "The Pylon Plugin has been disabled.");
        try
        {
            socketServer.sock.close();
        }
        catch(IOException ex)
        {
            Logger.getLogger(Pylon.class.getName()).log(Level.SEVERE, null, ex);
        }
        Pylon_DatabaseInterface.closeConnection(Pylon_DatabaseInterface.getConnection());
    }

    public void handleException(Exception ex)
    {
        getLogger().log(Level.SEVERE, null, ex);
        System.out.println("An exception has occurred in the Pylon Plugin, it is very likely this was an MySQL error, please check the logs for more details!");
    }
}
