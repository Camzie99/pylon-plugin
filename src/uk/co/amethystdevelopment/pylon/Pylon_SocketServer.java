package uk.co.amethystdevelopment.pylon;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.scheduler.BukkitRunnable;
import uk.co.amethystdevelopment.pylon.Pylon_Rank.Rank;

public class Pylon_SocketServer implements Runnable
{

    public ServerSocket sock;
    public static UUID currentCode;
    private Socket client;

    public Pylon_SocketServer()
    {
        try
        {
            sock = new ServerSocket(2606);
        }
        catch(IOException ex)
        {
            Logger.getLogger(Pylon_SocketServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void run()
    {
        while(true)
        {
            try
            {
                Socket clientSocket = sock.accept();
                PrintWriter out
                        = new PrintWriter(clientSocket.getOutputStream(), true);
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(clientSocket.getInputStream()));
                String ip = clientSocket.getInetAddress().getHostAddress();
                try
                {
                    if(!"127.0.0.1".equalsIgnoreCase(ip))
                    {
                        out.println("You are the wrong host, you are " + ip + " not 127.0.0.1");
                        close(out, in);
                        return;
                    }
                    String message = in.readLine();
                    String name = message.split("##SPLIT##")[0];
                    Rank rank = Pylon_Rank.levelToRank(Integer.parseInt(message.split("##SPLIT##")[1]));
                    String command = message.split("##SPLIT##")[2];
                    currentCode = UUID.randomUUID();
                    CommandSender sender = new Pylon_UtilitySender(name, currentCode.toString(), rank);
                    Bukkit.getServer().dispatchCommand(sender, command);
                    out.println("GOODBYE!");
                    close(out, in);
                }
                catch(IOException ex)
                {

                }
            }
            catch(IOException e)
            {

            }
        }
    }

    public static void close(final PrintWriter out, final BufferedReader in)
    {
        new BukkitRunnable()
        {
            @Override
            public void run()
            {
                try
                {
                    out.flush();
                    out.close();
                    in.close();
                }
                catch(IOException ex)
                {
                    Pylon.plugin.handleException(ex);
                }
            }
        }.runTaskLater(Pylon.plugin, 20L * 2L);

    }

}
