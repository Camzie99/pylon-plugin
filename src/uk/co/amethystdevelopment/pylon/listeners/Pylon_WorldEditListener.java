package uk.co.amethystdevelopment.pylon.listeners;

import java.util.ArrayList;
import me.StevenLawson.worldedit.LimitChangedEvent;
import me.StevenLawson.worldedit.SelectionChangedEvent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import uk.co.amethystdevelopment.pylon.Pylon_Rank;
import uk.co.amethystdevelopment.pylon.Pylon;
import uk.co.amethystdevelopment.pylon.protectedareas.Pylon_ProtectedArea;
import uk.co.amethystdevelopment.pylon.protectedareas.Pylon_ProtectedAreas;

//MASSIVE CREDIT TO TOTALFREEDOM FOR THIS!
public final class Pylon_WorldEditListener implements Listener
{

    public Pylon_WorldEditListener()
    {
        init();
    }
    
    public void init()
    {
        Bukkit.getPluginManager().registerEvents(this, Pylon.plugin);
    }

    @EventHandler
    public void onSelectionChange(final SelectionChangedEvent event)
    {
        final Player player = event.getPlayer();
        ArrayList<Pylon_ProtectedArea> areas = Pylon_ProtectedAreas.areasIn(event.getMinVector(), event.getMaxVector(), event.getWorld().getName());
        if(!areas.isEmpty())
        {
            for(Pylon_ProtectedArea area : areas)
            {
                if(!area.canAccess(player))
                {
                    player.sendMessage(ChatColor.RED + "The region that you selected contained a protected area which you do not have access to. Selection cleared.");
                    event.setCancelled(true);
                }
            }
        }
    }
    
    @EventHandler
    public void onLimitChanged(LimitChangedEvent event)
    {
        final Player player = event.getPlayer();

        if(Pylon_Rank.isAdmin(player))
        {
            return;
        }

        if(!event.getPlayer().equals(event.getTarget()))
        {
            player.sendMessage(ChatColor.RED + "Only admins can change the limit for other players!");
            event.setCancelled(true);
        }

        if(event.getLimit() < 0 || event.getLimit() > 50000)
        {
            event.setCancelled(true);
            player.sendMessage(ChatColor.RED + "You cannot set your limit higher than 50000 or to -1!");
        }
    }
}
