package uk.co.amethystdevelopment.pylon.listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import uk.co.amethystdevelopment.pylon.Pylon_Rank;
import uk.co.amethystdevelopment.pylon.Pylon;
import uk.co.amethystdevelopment.pylon.protectedareas.Pylon_ProtectedArea;
import uk.co.amethystdevelopment.pylon.protectedareas.Pylon_ProtectedAreas;

public final class Pylon_BlockListener implements Listener
{

    public Pylon_BlockListener()
    {
        init();
    }
    
    public void init()
    {
        Bukkit.getPluginManager().registerEvents(this, Pylon.plugin);
    }
    
    @EventHandler
    public void onPistonPush(BlockPistonExtendEvent event)
    {
        for(Block block : event.getBlocks())
        {
            if(block.getType() == Material.SLIME_BLOCK)
            {
                event.setCancelled(true);
            }
        }
    }
    
    @EventHandler
    public void onPistonPull(BlockPistonRetractEvent event)
    {
        for(Block block : event.getBlocks())
        {
            if(block.getType() == Material.SLIME_BLOCK)
            {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event)
    {
        Player player = event.getPlayer();
        for(Pylon_ProtectedArea area : Pylon_ProtectedAreas.getFromDatabase())
        {
            if(area.isInRange(event.getBlock().getLocation()))
            {
                if(!area.canAccess(player))
                {
                    player.sendMessage(ChatColor.RED + "You do not have permission to break blocks in this area! Please see " + area.getOwner() + " if you wish to gain access.");  
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event)
    {
        Player player = event.getPlayer();
        if(event.getBlock().getType() == Material.COMMAND && !Pylon_Rank.isOwner(player))
        {
            player.sendMessage(ChatColor.RED + "Only the owners can use command blocks.");
            event.setCancelled(true);
        }
        for(Pylon_ProtectedArea area : Pylon_ProtectedAreas.getFromDatabase())
        {
            if(area.isInRange(event.getBlock().getLocation()))
            {
                if(!area.canAccess(player))
                {
                    player.sendMessage(ChatColor.RED + "You do not have permission to place blocks in this area! Please see " + area.getOwner() + " if you wish to gain access.");  
                    event.setCancelled(true);
                }
            }
        }
    }
}
