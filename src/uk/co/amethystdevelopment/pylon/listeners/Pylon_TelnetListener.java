package uk.co.amethystdevelopment.pylon.listeners;

import me.StevenLawson.BukkitTelnet.api.TelnetCommandEvent;
import me.StevenLawson.BukkitTelnet.api.TelnetPreLoginEvent;
import uk.co.amethystdevelopment.pylon.Pylon_DatabaseInterface;
import uk.co.amethystdevelopment.pylon.Pylon_Rank;
import uk.co.amethystdevelopment.pylon.Pylon;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public final class Pylon_TelnetListener implements Listener
{

    public Pylon_TelnetListener()
    {
        if(!Bukkit.getPluginManager().isPluginEnabled("BukkitTelnet"))
        {
            Bukkit.broadcastMessage(ChatColor.RED + "BukkitTelnet cannot be found, disabling integration.");
            return;
        }
        init();
    }

    public void init()
    {
        Bukkit.getPluginManager().registerEvents(this, Pylon.plugin);
    }

    @EventHandler
    public void onTelnetPreLoginEvent(TelnetPreLoginEvent event)
    {
        String ip = event.getIp();
        if(Pylon_Rank.isEqualOrHigher(Pylon_Rank.getRankFromIp(ip), Pylon_Rank.Rank.SUPER))
        {
            event.setBypassPassword(true);
            event.setName("[" + Pylon_Rank.getNameFromIp(ip) + "]");
            Bukkit.broadcastMessage(ChatColor.DARK_GREEN + Pylon_Rank.getNameFromIp(ip) + " logged in via telnet.");
            for(Player player : Bukkit.getOnlinePlayers())
            {
                if(Pylon_Rank.isExecutive(player))
                {
                    player.sendMessage(ChatColor.DARK_GREEN + Pylon_Rank.getNameFromIp(ip) + " is on the IP of " + ip + ".");
                }
            }
        }
        else
        {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onTelnetCommand(TelnetCommandEvent event)
    {
        try
        {
            CommandSender player = event.getSender();
            for(Object result : Pylon_DatabaseInterface.getAsArrayList(null, null, "COMMAND", "COMMANDS"))
            {
                String blocked = (String) result;
                if(blocked.equalsIgnoreCase(event.getCommand().replaceAll("/", "")))
                {
                    if(!Pylon_Rank.isRankOrMore(player, (int) Pylon_DatabaseInterface.getFromTable("COMMAND", blocked, "RANK", "COMMANDS")))
                    {
                        player.sendMessage(ChatColor.RED + "You are not authorised to use this command.");
                        event.setCancelled(true);
                    }
                }
            }
            for(Player player2 : Bukkit.getOnlinePlayers())
            {
                if(Pylon_Rank.isSpecialist(player2))
                {
                    player2.sendMessage(ChatColor.GRAY + ChatColor.ITALIC.toString() + player.getName() + ": " + event.getCommand().toLowerCase());
                }
            }
        }
        catch(Exception ex)
        {
            Pylon.plugin.handleException(ex);
        }
    }
}
