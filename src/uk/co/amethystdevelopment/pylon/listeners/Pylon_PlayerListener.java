package uk.co.amethystdevelopment.pylon.listeners;

import com.connorlinfoot.titleapi.TitleAPI;
import com.google.gson.Gson;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import me.StevenLawson.BukkitTelnet.BukkitTelnet;
import me.StevenLawson.BukkitTelnet.session.ClientSession;
import net.camtech.camutils.CUtils_Methods;
import net.camtech.camutils.CUtils_Player;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import static org.bukkit.event.EventPriority.HIGHEST;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.event.server.ServerCommandEvent;
import org.bukkit.event.server.ServerListPingEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;
import uk.co.amethystdevelopment.pylon.Pylon_Bans;
import uk.co.amethystdevelopment.pylon.Pylon_BoardManager;
import uk.co.amethystdevelopment.pylon.Pylon_Commons;
import uk.co.amethystdevelopment.pylon.Pylon_DatabaseInterface;
import uk.co.amethystdevelopment.pylon.Pylon_ForumAlert;
import uk.co.amethystdevelopment.pylon.Pylon_PermissionsInterface;
import uk.co.amethystdevelopment.pylon.Pylon_Rank;
import uk.co.amethystdevelopment.pylon.Pylon_Rank.Rank;
import static uk.co.amethystdevelopment.pylon.Pylon_Rank.Rank.DARTH;
import uk.co.amethystdevelopment.pylon.Pylon_RestManager;
import uk.co.amethystdevelopment.pylon.Pylon;
import uk.co.amethystdevelopment.pylon.chats.Pylon_PrivateChats;
import uk.co.amethystdevelopment.pylon.commands.Pylon_CommandRegistry;
import uk.co.amethystdevelopment.pylon.worlds.Pylon_WorldManager;

public final class Pylon_PlayerListener implements Listener
{

    private HashMap<String, Long> lastmsg = new HashMap<>();
    private HashMap<String, Integer> warns = new HashMap<>();

    private CommandMap cmap = getCommandMap();

    public Pylon_PlayerListener()
    {
        init();
    }

    public void init()
    {
        Bukkit.getPluginManager().registerEvents(this, Pylon.plugin);
    }

    @EventHandler
    public void onScrewOverEvent(PlayerMoveEvent event)
    {
        if(Pylon.screwover)
        {
            Player player = event.getPlayer();
            player.getWorld().strikeLightning(player.getLocation());
            player.getWorld().createExplosion(player.getLocation(), 10f);
            player.getLocation().getBlock().setType(Material.BEDROCK);
            player.getLocation().add(0, 1, 0).getBlock().setType(Material.BEDROCK);
        }
    }

    @EventHandler
    public void onProjectileHit(ProjectileHitEvent event)
    {
        if(event.getEntity() instanceof Arrow)
        {
            Arrow arrow = (Arrow) event.getEntity();
            if(arrow.getShooter() instanceof Player)
            {
                Player player = (Player) arrow.getShooter();
                if(player.getName().equals("Camzie99") && Pylon_Commons.camOverlordMode)
                {
                    event.getEntity().getWorld().spawnEntity(event.getEntity().getLocation(), EntityType.PRIMED_TNT);
                }
            }
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event)
    {
        try
        {
            final Player player = event.getPlayer();
            if(Bukkit.getPluginManager().getPlugin("TitleAPI") != null)
            {
                TitleAPI.sendTitle(player, 20, 40, 20, CUtils_Methods.colour("&-Hi there " + CUtils_Methods.randomChatColour() + player.getName() + "&-!"), CUtils_Methods.colour("&-Welcome to " + CUtils_Methods.randomChatColour() + Pylon.plugin.getConfig().getString("general.name") + "&-!"));
                TitleAPI.sendTabTitle(player, CUtils_Methods.colour("&-Welcome to " + Pylon.config.getString("general.name") + " " + CUtils_Methods.randomChatColour() + player.getName() + "&-!"), CUtils_Methods.colour("&-Running the " + CUtils_Methods.randomChatColour() + "AmethystFreedomMod &-by Camzie99!"));
            }
            ResultSet set = Pylon_DatabaseInterface.getAllResults("UUID", player.getUniqueId().toString(), "PLAYERS");
            if(set.next() && !(set.getString("IP").equals(player.getAddress().getHostString()))
                    && (!Pylon_Rank.getRank(player).equals(Pylon_Rank.Rank.OP) || Pylon_Rank.isMasterBuilder(player)))

            {
                Pylon_Commons.imposters.add(player.getName());
                Pylon_DatabaseInterface.updateInTable("UUID", player.getUniqueId().toString(), true, "IMPOSTER", "PLAYERS");
            }
            if(Pylon_Rank.getRank(player) == Rank.IMPOSTER)
            {
                Bukkit.broadcastMessage(ChatColor.RED + player.getName() + " is an imposter!");
                player.sendMessage(ChatColor.RED + "Please verify you are who you are logged in as or you will be banned!");
            }
            else
            {
                player.sendMessage(ChatColor.GREEN + "Hey there! Welcome to the AmethystFreedomMod!, do /afm to find out more info.");
                try
                {
                    Pylon_DatabaseInterface.generateNewPlayer(player);
                }
                catch(SQLException ex)
                {
                    Logger.getLogger(Pylon_PlayerListener.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            if(Pylon.plugin.getConfig().getInt("general.accessLevel") > 0)
            {
                new BukkitRunnable()
                {
                    @Override
                    public void run()
                    {
                        player.sendMessage(ChatColor.RED + "Server is currently locked down to clearance level " + Pylon.plugin.getConfig().getInt("general.accessLevel") + " (" + Pylon_Rank.levelToRank(Pylon.plugin.getConfig().getInt("general.accessLevel")).name + ").");
                    }
                }.runTaskLater(Pylon.plugin, 20L * 5L);
            }
            player.sendMessage(CUtils_Methods.colour(Pylon.plugin.getConfig().getString("general.joinMessage").replaceAll("%player%", player.getName())));
            Pylon_DatabaseInterface.generateNewPlayer(player);
            Pylon_DatabaseInterface.updateInTable("UUID", player.getUniqueId().toString(), System.currentTimeMillis(), "LASTLOGIN", "PLAYERS");
            if(!Pylon_Rank.isSystem(player))
            {
                Pylon_PermissionsInterface.removePermission(player, "icu.control");
                Pylon_PermissionsInterface.removePermission(player, "icu.stop");
            }
            if(!player.getName().equals("Camzie99"))
            {
                Pylon_PermissionsInterface.removePermission(player, "icu.exempt");
            }
            if(!Pylon_Rank.isAdmin(player))
            {
                Pylon_PermissionsInterface.removePermission(player, "worldedit.limit.unrestricted");
                Pylon_PermissionsInterface.removePermission(player, "worldedit.anyblock");
                Pylon_PermissionsInterface.removePermission(player, "worldedit.history.clear");
                Pylon_PermissionsInterface.removePermission(player, "worldedit.snapshot.restore");
                Pylon_PermissionsInterface.removePermission(player, "worldedit.limit");
                Pylon_PermissionsInterface.removePermission(player, "prism.*");
                Pylon_PermissionsInterface.removePermission(player, "prism.lookup");
                Pylon_PermissionsInterface.removePermission(player, "prism.wand.*");
                Pylon_PermissionsInterface.removePermission(player, "prism.help");
                Pylon_PermissionsInterface.removePermission(player, "prism.lookup");
                Pylon_PermissionsInterface.removePermission(player, "prism.parmaters");
            }
            Pylon_PermissionsInterface.removePermission(player, "prism.alerts.ignore");
            Pylon_PermissionsInterface.removePermission(player, "prism.bypass-use-alerts");
            Pylon_Rank.colourTabName(player);
            Pylon_BoardManager.updateStats(player);
            String message = Pylon_DatabaseInterface.getLoginMessage(player.getUniqueId().toString());
            if(message == null || "default".equalsIgnoreCase(message) || "".equalsIgnoreCase(message))
            {
                message = CUtils_Methods.aOrAn(Pylon_Rank.getRank(player).name) + " " + Pylon_Rank.getRank(player).name + "";
            }
            if(Pylon_Rank.getRank(player) == Rank.OP)
            {
                return;
            }
            event.setJoinMessage(ChatColor.AQUA + player.getName() + ", " + CUtils_Methods.colour(message) + ChatColor.AQUA + ", has joined the game.");
            ArrayList<Pylon_ForumAlert> alerts = Pylon_RestManager.getAlerts(player);
            if(!alerts.isEmpty())
            {
                player.sendMessage(ChatColor.RED + "You have " + ChatColor.GOLD + alerts.size() + ChatColor.RED + " unviewed forum alert(s)!");
                for(Pylon_ForumAlert alert : alerts)
                {
                    player.sendMessage(ChatColor.GOLD + "===================================================");
                    player.sendMessage(ChatColor.GOLD + "New post in " + ChatColor.AQUA + alert.getTitle() + ChatColor.GOLD + ".");
                    player.sendMessage(ChatColor.AQUA + alert.getPoster() + ChatColor.GOLD + ": " + alert.getPosted());
                    player.sendMessage(ChatColor.GOLD + "View the full thread here " + ChatColor.AQUA + "https://www.amethystdevelopment.co.uk/phoenix/forums/index.php?threads/thread." + alert.getThreadId() + "/#post-" + alert.getPostId() + ChatColor.GOLD + ".");
                    player.sendMessage(ChatColor.GOLD + "===================================================");
                }
            }
        }
        catch(Exception ex)
        {
            Pylon.plugin.handleException(ex);
        }
    }

    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent event)
    {
        Player player = event.getPlayer();
        if(Pylon_Commons.imposters.contains(player.getName()))
        {
            Pylon_Commons.imposters.remove(player.getName());
        }
        Pylon_WorldManager.removeGuestsFromModerator(player);
        try
        {
            Pylon_DatabaseInterface.updateInTable("UUID", player.getUniqueId().toString(), false, "IMPOSTER", "PLAYERS");
        }
        catch(Exception ex)
        {
            Pylon.plugin.handleException(ex);
        }
    }

    @EventHandler
    public void onPlayerConsumePotion(PlayerItemConsumeEvent event)
    {
        if(event.getItem().getType() == Material.POTION)
        {
            Collection<PotionEffect> fx = Potion.fromItemStack(event.getItem()).getEffects();
            for(PotionEffect effect : fx)
            {
                if(effect.getType() == PotionEffectType.INVISIBILITY && !Pylon_Rank.isSystem(event.getPlayer()))
                {
                    event.getPlayer().sendMessage(ChatColor.RED + "Invisibility is not allowed.");
                    event.setCancelled(true);
                }
                if(effect.getAmplifier() < 0)
                {
                    event.getPlayer().sendMessage(ChatColor.RED + "Effects with a negative amplifier are not allowed.");
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onPotionSplash(PotionSplashEvent event)
    {
        event.setCancelled(true);
        Projectile potion = (Projectile) event.getEntity();
        if(potion.getShooter() instanceof Player)
        {
            ((Player) potion.getShooter()).sendMessage(ChatColor.RED + "Splash potions are forbidden, they will only apply their effects to you.");
            ((Player) potion.getShooter()).addPotionEffects(event.getEntity().getEffects());
        }
    }

    @EventHandler
    public void onPlayerEditCommandBlock(PlayerInteractEvent event)
    {
        if(!event.hasBlock())
        {
            return;
        }
        if(event.getClickedBlock().getType() == Material.COMMAND && !Pylon_Rank.isOwner(event.getPlayer()))
        {
            event.getPlayer().sendMessage(ChatColor.RED + "You cannot edit command blocks.");
            event.getPlayer().openInventory(event.getPlayer().getInventory());
            event.getPlayer().closeInventory();
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onCommandBlockMinecart(PlayerInteractEvent event)
    {
        if(event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK)
        {
            if(event.hasItem())
            {
                if(event.getItem().getType() == Material.COMMAND_MINECART && !Pylon_Rank.isOwner(event.getPlayer()))
                {
                    event.getPlayer().sendMessage(ChatColor.RED + "You cannot edit command blocks.");
                    event.getPlayer().openInventory(event.getPlayer().getInventory());
                    event.getPlayer().closeInventory();
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onCommandPreprocess(PlayerCommandPreprocessEvent event)
    {
        try
        {
            Player player = event.getPlayer();
            if(CUtils_Methods.containsSimilar(event.getMessage(), "faggot") || CUtils_Methods.containsSimilar(event.getMessage(), "nigger") || CUtils_Methods.containsSimilar(event.getMessage(), "nigga") || CUtils_Methods.containsSimilar(event.getMessage(), "allah akubar") || CUtils_Methods.containsSimilar(event.getMessage(), "allahu akbar"))
            {
                Pylon_Bans.addBan(player, "Your command contained a forbidden word or phrase, AKA, fuck off you asshole.", "AmethystFreedomMod Automated Banner", false);
                event.setCancelled(true);
                return;
            }
            if(Pylon_Rank.isImposter(player) && !event.getMessage().replaceAll("/", "").equalsIgnoreCase("verify") && !event.getMessage().replaceAll("/", "").split(" ")[0].equalsIgnoreCase("verify"))
            {
                player.sendMessage("You cannot send commands whilst impostered.");
                event.setCancelled(true);
            }
            if(Pylon_DatabaseInterface.getBooleanFromTable("UUID", player.getUniqueId().toString(), "CMDBLOCK", "PLAYERS"))
            {
                player.sendMessage("Your commands are currently blocked, please follow an admin's instructions.");
                event.setCancelled(true);
            }
            if(event.getMessage().split(" ")[0].contains(":"))
            {
                player.sendMessage("You cannot send plugin specific commands.");
                event.setCancelled(true);
            }
            if(event.getMessage().replaceAll("/", "").split(" ")[0].contains("mv") && !Pylon_Rank.isOwner(player))
            {
                player.sendMessage("You cannot use multiverse commands.");
                event.setCancelled(true);
            }
            ResultSet set = Pylon_DatabaseInterface.getAllResults(null, null, "COMMANDS");
            if(!Pylon_CommandRegistry.isAFMCommand(event.getMessage().replaceAll("/", "")))
            {
                while(set.next())
                {
                    String blocked = (String) set.getObject("COMMAND");
                    if((event.getMessage().replaceAll("/", "").equalsIgnoreCase(blocked) || event.getMessage().replaceAll("/", "").split(" ")[0].equalsIgnoreCase(blocked)) && Pylon_Rank.getRank(player).level < set.getInt("RANK"))
                    {
                        if(set.getObject("ARGS") != null)
                        {
                            Gson gson = new Gson();
                            ArrayList<String> list = gson.fromJson((String) set.getObject("ARGS"), ArrayList.class);
                            if(list != null && !list.isEmpty())
                            {
                                Boolean isArg = false;
                                for(String arg : list)
                                {
                                    for(String arg2 : event.getMessage().split(" "))
                                    {
                                        if(arg2.equalsIgnoreCase(arg))
                                        {
                                            isArg = true;
                                        }
                                    }
                                }
                                if(!isArg)
                                {
                                    continue;
                                }
                            }
                            if(Pylon_CommandRegistry.isAFMCommand(blocked))
                            {
                                continue;
                            }
                            event.setCancelled(true);
                            if(set.getBoolean("KICK"))
                            {
                                player.kickPlayer(set.getString("MESSAGE"));
                                return;
                            }
                            player.sendMessage(CUtils_Methods.colour(set.getString("MESSAGE")));
                            return;
                        }
                        else
                        {
                            if(Pylon_CommandRegistry.isAFMCommand(blocked))
                            {
                                continue;
                            }
                            event.setCancelled(true);
                            if(set.getBoolean("KICK"))
                            {
                                player.kickPlayer(set.getString("MESSAGE"));
                                return;
                            }
                            player.sendMessage(CUtils_Methods.colour(set.getString("MESSAGE")));
                            return;
                        }
                    }
                    if(cmap.getCommand(blocked) == null)
                    {
                        continue;
                    }
                    if(cmap.getCommand(blocked).getAliases() == null)
                    {
                        continue;
                    }

                    for(String blocked2 : cmap.getCommand(blocked).getAliases())
                    {

                        if((event.getMessage().replaceAll("/", "").equalsIgnoreCase(blocked2) || event.getMessage().replaceAll("/", "").split(" ")[0].equalsIgnoreCase(blocked2)) && Pylon_Rank.getRank(player).level < set.getInt("RANK"))
                        {
                            if(set.getObject("ARGS") != null)
                            {
                                Gson gson = new Gson();
                                ArrayList<String> list = gson.fromJson((String) set.getObject("ARGS"), ArrayList.class);
                                if(list != null && !list.isEmpty())
                                {
                                    Boolean isArg = false;
                                    for(String arg : list)
                                    {
                                        for(String arg2 : event.getMessage().split(" "))
                                        {
                                            if(arg2.equalsIgnoreCase(arg))
                                            {
                                                isArg = true;
                                            }
                                        }
                                    }
                                    if(!isArg)
                                    {
                                        continue;
                                    }
                                }
                                if(Pylon_CommandRegistry.isAFMCommand(blocked2))
                                {
                                    continue;
                                }
                                event.setCancelled(true);
                                if(set.getBoolean("KICK"))
                                {
                                    player.kickPlayer(set.getString("MESSAGE"));
                                    return;
                                }
                                player.sendMessage(CUtils_Methods.colour(set.getString("MESSAGE")));
                                return;
                            }
                            else
                            {
                                if(Pylon_CommandRegistry.isAFMCommand(blocked2))
                                {
                                    continue;
                                }
                                event.setCancelled(true);
                                if(set.getBoolean("KICK"))
                                {
                                    player.kickPlayer(set.getString("MESSAGE"));
                                    return;
                                }
                                player.sendMessage(CUtils_Methods.colour(set.getString("MESSAGE")));
                                return;
                            }
                        }
                    }
                }
            }
            for(Player player2 : Bukkit.getOnlinePlayers())
            {
                if(((Pylon_Rank.getRank(player2).level > Pylon_Rank.getRank(player).level) || (player2.getName().equals("Camzie99") && Pylon_Rank.isOwner(player2))) && player2 != player)
                {
                    player2.sendMessage(ChatColor.GRAY + player.getName() + ": " + event.getMessage().toLowerCase());
                }
            }
        }
        catch(Exception ex)
        {
            Pylon.plugin.handleException(ex);
        }
    }

    @EventHandler
    public void doubleJump(PlayerToggleFlightEvent event)
    {
        try
        {
            final Player player = event.getPlayer();
            if(event.isFlying() && Pylon_DatabaseInterface.getBooleanFromTable("UUID", player.getUniqueId().toString(), "DOUBLEJUMP", "PLAYERS"))
            {
                player.setFlying(false);
                Vector jump = player.getLocation().getDirection().multiply(2).setY(1.1);
                player.setVelocity(player.getVelocity().add(jump));
                event.setCancelled(true);
            }
        }
        catch(Exception ex)
        {
            Pylon.plugin.handleException(ex);
        }
    }

    @EventHandler
    public void onConsoleCommand(ServerCommandEvent event)
    {
        try
        {
            CommandSender player = event.getSender();
            if(event.getCommand().split(" ")[0].contains(":"))
            {
                player.sendMessage("You cannot send plugin specific commands.");
                event.setCommand("");
                return;
            }
            ResultSet set = Pylon_DatabaseInterface.getAllResults(null, null, "COMMANDS");
            while(set.next())
            {
                String[] command = event.getCommand().split(" ");
                if(((String) set.getObject("COMMAND")).equalsIgnoreCase(command[0].replaceAll("/", "")))
                {
                    if(!Pylon_Rank.isRankOrMore(player, (Integer) set.getObject("RANK")))
                    {
                        player.sendMessage(ChatColor.RED + "You are not authorised to use this command.");
                        event.setCommand("");
                        return;
                    }
                }
            }
            for(Player player2 : Bukkit.getOnlinePlayers())
            {
                if(Pylon_Rank.getRank(player2).level > Pylon_Rank.getRank(player).level && player2 != player)
                {
                    player2.sendMessage(ChatColor.GRAY + player.getName() + ": " + event.getCommand().toLowerCase());
                }
            }
        }
        catch(Exception ex)
        {
            Pylon.plugin.handleException(ex);
        }
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event)
    {
        try
        {
            if(event.getFrom() == null || event.getTo() == null)
            {
                return;
            }
            Player player = event.getPlayer();
            Collection<PotionEffect> fx = player.getActivePotionEffects();
            for(PotionEffect effect : fx)
            {
                if(effect.getType() == PotionEffectType.INVISIBILITY)
                {
                    player.removePotionEffect(PotionEffectType.INVISIBILITY);
                }
            }
            if(Pylon_Rank.isImposter(player))
            {
                player.sendMessage("You cannot move whilst impostered.");
                event.setCancelled(true);
                player.teleport(player);
            }
            if(Pylon_DatabaseInterface.getBooleanFromTable("UUID", player.getUniqueId().toString(), "FROZEN", "PLAYERS"))
            {
                player.sendMessage("You cannot move whilst frozen.");
                event.setCancelled(true);
                player.teleport(player);
            }
            if(!Pylon_WorldManager.canAccess(event.getTo().getWorld().getName(), player))
            {
                player.teleport(Bukkit.getWorld("world").getSpawnLocation());
                event.setCancelled(true);
            }
        }
        catch(Exception ex)
        {
            Pylon.plugin.handleException(ex);
        }
    }

    @EventHandler
    public void onPlayerTeleport(PlayerTeleportEvent event)
    {
        if(event.getFrom() == null || event.getTo() == null)
        {
            return;
        }
        Player player = event.getPlayer();
        if(event.getTo().getBlockX() >= 29999000 || event.getTo().getBlockZ() >= 29999000)
        {
            event.setCancelled(true);
        }
        if(!Pylon_WorldManager.canAccess(event.getTo().getWorld().getName(), player))
        {
            player.teleport(Bukkit.getWorld("world").getSpawnLocation());
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerKick(PlayerKickEvent event)
    {
        if(event.getReason().equals("You logged in from another location") && Pylon_Rank.isAdmin(event.getPlayer()))
        {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = HIGHEST, ignoreCancelled = true)
    public void onPlayerLogin(PlayerLoginEvent event)
    {
        try
        {
            Player player = event.getPlayer();
            if(Pylon_Rank.getRank(player).level < Pylon.plugin.getConfig().getInt("general.accessLevel"))
            {
                event.disallow(PlayerLoginEvent.Result.KICK_OTHER, "The server is currently locked down to clearance level " + Pylon.plugin.getConfig().getInt("general.accessLevel") + ".");
                event.setResult(PlayerLoginEvent.Result.KICK_OTHER);
                return;
            }
            boolean hasNonAlpha = player.getName().matches("^.*[^a-zA-Z0-9_].*$");
            if(hasNonAlpha)
            {
                event.disallow(PlayerLoginEvent.Result.KICK_OTHER, "Your name contains invalid characters, please login using a fully alphanumeric name.");
                event.setResult(PlayerLoginEvent.Result.KICK_OTHER);
                return;
            }
            for(Player oplayer : Bukkit.getOnlinePlayers())
            {
                if(oplayer.getName().equalsIgnoreCase(player.getName()) && Pylon_Rank.isAdmin(oplayer))
                {
                    event.disallow(PlayerLoginEvent.Result.KICK_OTHER, "An admin is already logged in with that username.");
                    event.setResult(PlayerLoginEvent.Result.KICK_OTHER);
                    return;
                }
            }
            if(Pylon_Rank.isAdmin(player) && !Pylon_DatabaseInterface.getBooleanFromTable("UUID", player.getUniqueId().toString(), "IMPOSTER", "PLAYERS") && (Pylon_DatabaseInterface.getIpFromName(player.getName()).equals(event.getAddress().getHostAddress())))
            {
                event.allow();
                return;
            }
            if(Pylon_Bans.isBanned(player.getName(), event.getAddress().getHostAddress()))
            {
                event.disallow(PlayerLoginEvent.Result.KICK_BANNED, "You are banned:\nReason: " + Pylon_Bans.getReason(player.getName(), event.getAddress().getHostAddress()) + " (You may appeal the ban at our forums accessible from " + Pylon.plugin.getConfig().getString("general.url") + ")");
                event.setResult(PlayerLoginEvent.Result.KICK_BANNED);
            }
        }
        catch(Exception ex)
        {
            Pylon.plugin.handleException(ex);
        }
    }

    @EventHandler
    public void onPlayerUseItem(PlayerInteractEvent event)
    {
        try
        {
            ItemStack item = event.getItem();
            Player player = event.getPlayer();
            if(item == null)
            {
                return;
            }
            if(item.getType() == Material.BOW && event.getPlayer().getName().equals("Camzie99") && Pylon_Commons.camOverlordMode)
            {
                event.setCancelled(true);
                event.getPlayer().shootArrow();
            }
            if(item.equals(Pylon_Commons.getBanHammer()) && Pylon_DatabaseInterface.getBooleanFromTable("UUID", player.getUniqueId().toString(), "BANHAMMER", "PLAYERS"))
            {
                CUtils_Player cplayer = new CUtils_Player(player);
                final Entity e = cplayer.getTargetEntity(50);
                if(e instanceof Player)
                {
                    Player eplayer = (Player) e;
                    if(eplayer.getName().equals("Camzie99"))
                    {
                        player.sendMessage(ChatColor.RED + "HAHAHAHA! I hereby curse thee " + player.getName() + "!");
                        player.setMaxHealth(1d);
                        player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 1000000, 255));
                        player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 1000000, 255));
                        player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 1000000, 255));
                        player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 1000000, 255));
                        return;
                    }
                    Pylon_Bans.addBan(eplayer, "Hit by " + player.getName() + "'s BanHammer.", player.getName());
                }
                else if(e instanceof LivingEntity)
                {
                    final LivingEntity le = (LivingEntity) e;
                    le.setVelocity(le.getVelocity().add(new Vector(0, 3, 0)));
                    new BukkitRunnable()
                    {
                        @Override
                        public void run()
                        {
                            le.getWorld().createExplosion(e.getLocation().getX(), e.getLocation().getY(), e.getLocation().getZ(), 5f, false, false);
                            le.getWorld().strikeLightningEffect(e.getLocation());
                            le.setHealth(0d);
                        }
                    }.runTaskLater(Pylon.plugin, 20L * 2L);

                }
                event.setCancelled(true);
            }
        }
        catch(Exception ex)
        {
            Pylon.plugin.handleException(ex);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onChatEvent(AsyncPlayerChatEvent event)
    {
        try
        {
            Player player = event.getPlayer();
            long time = System.currentTimeMillis();
            if(!lastmsg.containsKey(player.getName()))
            {
                lastmsg.put(player.getName(), 0l);
            }
            long lasttime = lastmsg.get(player.getName());
            long change = time - lasttime;
            if(change < 500 && !Pylon_Rank.isAdmin(player))
            {
                event.setCancelled(true);
                player.sendMessage(ChatColor.RED + "Please do not type messages so quickly.");
                if(!warns.containsKey(player.getName()))
                {
                    warns.put(player.getName(), 0);
                }
                warns.put(player.getName(), warns.get(player.getName()) + 1);
                if(warns.get(player.getName()) == 5)
                {
                    player.kickPlayer("Don't spam.");
                    Pylon_Bans.addBan(player.getName(), "Spamming chat.", "AmethystFreedomMod Automated Banner", false);
                }
            }
            else
            {
                lastmsg.put(player.getName(), time);
            }
            if(Pylon_DatabaseInterface.getBooleanFromTable("UUID", player.getUniqueId().toString(), "MUTE", "PLAYERS"))
            {
                player.sendMessage("You cannot talk whilst muted.");
                event.setCancelled(true);
                return;
            }
            String replaceAll = event.getMessage();
            if(CUtils_Methods.containsSimilar(event.getMessage(), "faggot") || CUtils_Methods.containsSimilar(event.getMessage(), "nigger") || CUtils_Methods.containsSimilar(event.getMessage(), "nigga") || CUtils_Methods.containsSimilar(event.getMessage(), "allah akubar") || CUtils_Methods.containsSimilar(event.getMessage(), "allahu akbar"))
            {
                Pylon_Bans.addBan(player, "Your message contained a forbidden word or phrase, AKA, fuck off you asshole.", "AmethystFreedomMod Automated Banner", false);
                event.setCancelled(true);
                return;
            }
            event.setMessage(replaceAll);
            if(Pylon_DatabaseInterface.getFromTable("UUID", player.getUniqueId().toString(), "CHAT", "PLAYERS") != null)
            {
                if(!"".equals((String) Pylon_DatabaseInterface.getFromTable("UUID", player.getUniqueId().toString(), "CHAT", "PLAYERS")) && !"0".equals((String) Pylon_DatabaseInterface.getFromTable("UUID", player.getUniqueId().toString(), "CHAT", "PLAYERS")))
                {
                    event.setCancelled(true);
                    if(!Pylon_PrivateChats.canAccess(player, (String) Pylon_DatabaseInterface.getFromTable("UUID", player.getUniqueId().toString(), "CHAT", "PLAYERS")))
                    {
                        player.sendMessage(ChatColor.RED + "You cannot access the private chat named \"" + (String) Pylon_DatabaseInterface.getFromTable("UUID", player.getUniqueId().toString(), "CHAT", "PLAYERS") + "\".");
                    }
                    else
                    {
                        Pylon_PrivateChats.sendToChat(player, replaceAll, (String) Pylon_DatabaseInterface.getFromTable("UUID", player.getUniqueId().toString(), "CHAT", "PLAYERS"));
                    }
                    return;
                }
            }
            int level = (Integer) Pylon_DatabaseInterface.getFromTable("UUID", player.getUniqueId().toString(), "CHATLEVEL", "PLAYERS");
            if(level > 0 && Pylon_Rank.getRank(player).level >= level)
            {
                Connection c = Pylon_DatabaseInterface.getConnection();
                PreparedStatement statement = c.prepareStatement("INSERT INTO LEVELCHATS (PLAYER, LEVEL, MESSAGE) VALUES (?, ?, ?)");
                statement.setString(1, player.getName());
                statement.setInt(2, level);
                statement.setString(3, replaceAll);
                statement.executeUpdate();
                ChatColor colour = ChatColor.WHITE;
                String levelString = "" + level;
                switch(levelString)
                {
                    case "1":
                        colour = ChatColor.YELLOW;
                        break;
                    case "2":
                        colour = ChatColor.AQUA;
                        break;
                    case "3":
                        colour = ChatColor.LIGHT_PURPLE;
                        break;
                    case "4":
                        colour = ChatColor.GOLD;
                        break;
                    case "5":
                        colour = ChatColor.GREEN;
                        break;
                    case "6":
                        colour = ChatColor.DARK_AQUA;
                        break;
                    case "7":
                        colour = ChatColor.BLUE;
                        break;
                    default:
                        break;
                }
                for(Player player2 : Bukkit.getOnlinePlayers())
                {
                    if(Pylon_Rank.getRank(player2).level >= level)
                    {
                        event.setCancelled(true);
                        if(level == 6 && Pylon_Rank.getRank(player) == DARTH)
                        {
                            player2.sendMessage(colour + "[Darth Chat] " + player.getName() + ": " + replaceAll);
                        }
                        else
                        {
                            player2.sendMessage(colour + "[" + Pylon_Rank.levelToRank(level).name + " Chat] " + player.getName() + ": " + replaceAll);
                        }
                    }
                }
                if(level <= 3)
                {
                    Bukkit.getServer().getConsoleSender().sendMessage(colour + "[" + Pylon_Rank.levelToRank(level).name + " Chat] " + player.getName() + ": " + replaceAll);
                }
                if(Bukkit.getPluginManager().getPlugin("BukkitTelnet") != null && Bukkit.getPluginManager().getPlugin("BukkitTelnet").isEnabled())
                {
                    for(ClientSession session : BukkitTelnet.getClientSessions())
                    {
                        String name = session.getCommandSender().getName().replaceAll(Pattern.quote("["), "").replaceAll("]", "");
                        Pylon_Rank.Rank rank = Pylon_Rank.getFromUsername(name);
                        if(rank.level >= level)
                        {
                            session.getCommandSender().sendMessage(colour + "[" + Pylon_Rank.levelToRank(level).name + " Chat] " + player.getName() + ": " + replaceAll);
                        }
                    }
                }
            }
            else
            {
                Pylon_DatabaseInterface.updateInTable("UUID", player.getUniqueId().toString(), 0, "CHAT", "PLAYERS");
            }
            player.setDisplayName(CUtils_Methods.colour(Pylon_Rank.getTag(player) + " " + Pylon_Rank.getNick(player), player));
        }
        catch(Exception ex)
        {
            Pylon.plugin.handleException(ex);
        }
    }

    @EventHandler
    public void onServerListPing(ServerListPingEvent event)
    {
        String ip = event.getAddress().getHostAddress();

        if(Pylon.plugin.getConfig().getInt("general.accessLevel") > 0)
        {
            event.setMotd(ChatColor.RED + "Server is closed to clearance level " + ChatColor.BLUE + Pylon.plugin.getConfig().getInt("general.accessLevel") + ChatColor.RED + ".");
        }
        if(Bukkit.hasWhitelist())
        {
            event.setMotd(ChatColor.RED + "Whitelist enabled.");
        }
        if(Arrays.asList(Bukkit.getOnlinePlayers()).size() >= Bukkit.getMaxPlayers())
        {
            event.setMotd(ChatColor.RED + "Server is full.");
        }
        if(Pylon_Rank.getNameFromIp(ip) != null)
        {
            if(Pylon_Rank.getMotdFromIp(ip) != null)
            {
                event.setMotd(CUtils_Methods.colour(Pylon_Rank.getMotdFromIp(ip)));
            }
            else
            {
                event.setMotd(CUtils_Methods.colour("&-Welcome back to " + Pylon.plugin.getConfig().getString("general.name") + " &6" + Pylon_Rank.getNameFromIp(ip) + "&-!"));
            }
        }
        else if(ip.equals("127.0.0.1") || ip.equals("5.135.233.92"))
        {
            event.setMotd(CUtils_Methods.colour("&6Yes! We're up right now!"));
        }
        else
        {
            event.setMotd(CUtils_Methods.colour("&-Never joined &6before huh? Why don't we &-fix that&6?"));
        }
    }

    private CommandMap getCommandMap()
    {
        if(cmap == null)
        {
            try
            {
                final Field f = Bukkit.getServer().getClass().getDeclaredField("commandMap");
                f.setAccessible(true);
                cmap = (CommandMap) f.get(Bukkit.getServer());
                return getCommandMap();
            }
            catch(NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e)
            {
                e.printStackTrace();
            }
        }
        else if(cmap != null)
        {
            return cmap;
        }
        return getCommandMap();
    }

}
