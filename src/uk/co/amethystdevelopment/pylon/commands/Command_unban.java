package uk.co.amethystdevelopment.pylon.commands;

import uk.co.amethystdevelopment.pylon.Pylon_Bans;
import uk.co.amethystdevelopment.pylon.Pylon_Rank;
import uk.co.amethystdevelopment.pylon.Pylon_Rank.Rank;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command_unban extends Pylon_Command
{

    public Command_unban()
    {
        super("unban", "/unban [player]", "Unan a player.", "You don't have permission to use this command.", Rank.ADMIN);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if (args.length != 1)
        {
            return false;
        }
        Player player = Pylon_Rank.getPlayer(args[0]);
        if (player == null)
        {
            Bukkit.broadcastMessage(ChatColor.RED + sender.getName() + " - Attempting unban of offline player: " + args[0]);
            Pylon_Bans.unBan(args[0]);
        }
        else
        {
            Bukkit.broadcastMessage(ChatColor.RED + sender.getName() + " - Attempting to unban player: " + player.getName());
            Pylon_Bans.unBan(player);
        }
        return true;
    }

}
