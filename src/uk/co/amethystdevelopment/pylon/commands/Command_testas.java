package uk.co.amethystdevelopment.pylon.commands;

import uk.co.amethystdevelopment.pylon.Pylon_Rank;
import uk.co.amethystdevelopment.pylon.Pylon_Rank.Rank;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@CommandParameters(name="testas", usage="/testas <rank> <command to test>", description="Test a command as a different rank.")
public class Command_testas
{
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(args.length < 2)
        {
            return false;
        }
        if(!(sender instanceof Player))
        {
            sender.sendMessage(ChatColor.RED + "Only in-game players can use this command.");
            return true;
        }
        int level;
        try
        {
            level = Integer.parseInt(args[0]);
        }
        catch(NumberFormatException ex)
        {
            sender.sendMessage(ChatColor.RED + "The rank must be an integer lower than your current rank.");
            return false;
        }
        Rank trueRank = Pylon_Rank.getRank(sender);
        if(level >= trueRank.level)
        {
            sender.sendMessage(ChatColor.RED + "The rank must be an integer lower than your current rank.");
            return true;
        }
        String command = StringUtils.join(ArrayUtils.subarray(args, 1, args.length), " ");
        Pylon_Rank.setRank((Player) sender, Pylon_Rank.levelToRank(level), null);
        ((Player) sender).chat("/" + command);
        Pylon_Rank.setRank((Player) sender, trueRank, null);
        return true;
    }
}