package uk.co.amethystdevelopment.pylon.commands;

import uk.co.amethystdevelopment.pylon.Pylon_Rank;
import uk.co.amethystdevelopment.pylon.Pylon_Rank.Rank;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command_deopall extends Pylon_Command
{
    public Command_deopall()
    {
        super("deopall", "/deopall", "Deop all players on the server.", Rank.SUPER);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if (!Pylon_Rank.isSuper(sender))
        {
            sender.sendMessage(ChatColor.RED + "You don't have permissions to execute this command.");
            return true;
        }

        for (Player player : Bukkit.getOnlinePlayers())
        {
            if (!Pylon_Rank.isAdmin(player))
            {
                player.setOp(false);
            }
        }
        return true;
    }

}
