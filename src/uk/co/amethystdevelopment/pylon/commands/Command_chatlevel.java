package uk.co.amethystdevelopment.pylon.commands;

import java.util.Arrays;
import uk.co.amethystdevelopment.pylon.Pylon_BoardManager;
import uk.co.amethystdevelopment.pylon.Pylon_DatabaseInterface;
import uk.co.amethystdevelopment.pylon.Pylon_Rank;
import uk.co.amethystdevelopment.pylon.Pylon_Rank.Rank;
import uk.co.amethystdevelopment.pylon.Pylon;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command_chatlevel extends Pylon_Command
{

    public Command_chatlevel()
    {
        super("chatlevel", "/chatlevel [level]", "Change your chat level.", Arrays.asList("cl"), Rank.ADMIN);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        try
        {
        if (args.length < 1)
        {
            return false;
        }
        if (!(sender instanceof Player))
        {
            sender.sendMessage(ChatColor.GREEN + "You must be in-game to use this command.");
            return true;
        }
        Player player = (Player) sender;
        int oldlevel = (Integer) Pylon_DatabaseInterface.getFromTable("UUID", player.getUniqueId().toString(), "CHATLEVEL", "PLAYERS");
        int level;
        try
        {
            level = Integer.parseInt(args[0]);
        } catch (Exception ex)
        {
            level = Pylon_Rank.nameToRank(StringUtils.join(ArrayUtils.subarray(args, 0, args.length), " ")).level;
        }
        if (Pylon_Rank.getRank(sender).level >= level)
        {
            Pylon_DatabaseInterface.updateInTable("UUID", player.getUniqueId().toString(), level, "CHATLEVEL", "PLAYERS");
            sender.sendMessage(ChatColor.GREEN + "You are now talking in " + Pylon_Rank.levelToRank(level).name + " Chat.");
        }
        else
        {
            sender.sendMessage(ChatColor.RED + "You do not have permission to access this chat level!");
        }
        if(args.length > 1)
        {
            String msg =  StringUtils.join(ArrayUtils.subarray(args, 1, args.length), " ");
            player.chat(msg);
            Pylon_DatabaseInterface.updateInTable("UUID", player.getUniqueId().toString(), oldlevel, "CHATLEVEL", "PLAYERS");
        }
        Pylon_BoardManager.updateStats(player);
        }
        catch(Exception ex)
        {
            Pylon.plugin.handleException(ex);
        }
        return true;
    }

}
