package uk.co.amethystdevelopment.pylon.commands;

import net.camtech.camutils.CUtils_Methods;
import uk.co.amethystdevelopment.pylon.Pylon_Rank;
import uk.co.amethystdevelopment.pylon.Pylon_Rank.Rank;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@CommandParameters(name="kick", description="Kick a player.", usage="/kick [player] <reason>", rank=Rank.ADMIN)
public class Command_kick extends Pylon_Command
{

    public Command_kick()
    {
        super("kick", "/kick [player] <reason>", "Kick a player.", Rank.ADMIN);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if (args.length < 1)
        {
            return false;
        }
        if (args.length == 1)
        {
            Player player = Pylon_Rank.getPlayer(args[0]);
            if (player == null)
            {
                sender.sendMessage("The player " + args[0] + " is not online.");
                return true;
            }
            if (Pylon_Rank.getRank(sender).level > Pylon_Rank.getRank(player).level)
            {
                player.kickPlayer("You have been kicked by " + sender.getName());
                return true;
            }
            sender.sendMessage(ChatColor.RED + "You cannot kick someone of an equal or higher rank than yourself.");
            return true;
        }
        else
        {
            String message = CUtils_Methods.colour(StringUtils.join(ArrayUtils.subarray(args, 1, args.length), " "));
            Player player = Pylon_Rank.getPlayer(args[0]);
            if (player == null)
            {
                sender.sendMessage("The player " + args[0] + " is not online.");
                return true;
            }
            if (Pylon_Rank.getRank(sender).level > Pylon_Rank.getRank(player).level)
            {
                player.kickPlayer(message + " - " + sender.getName());
                return true;
            }
            sender.sendMessage(ChatColor.RED + "You cannot kick someone of an equal or higher rank than yourself.");
            return true;
        }
    }

}
