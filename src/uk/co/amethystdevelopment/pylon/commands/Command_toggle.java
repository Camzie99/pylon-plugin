package uk.co.amethystdevelopment.pylon.commands;

import uk.co.amethystdevelopment.pylon.Pylon_Rank.Rank;
import uk.co.amethystdevelopment.pylon.Pylon;
import uk.co.amethystdevelopment.pylon.listeners.Pylon_ToggleableEventsListener;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class Command_toggle extends Pylon_Command
{
    public Command_toggle()
    {
        super("toggle", "/toggle [value]", "Toggle a server setting.", Rank.ADMIN);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if (args.length == 0)
        {
            sender.sendMessage(ChatColor.AQUA + "Possible toggles:");
            for (String toggle : Pylon.plugin.getConfig().getConfigurationSection("toggles").getKeys(false))
            {
                sender.sendMessage(" - " + (Pylon.plugin.getConfig().getBoolean("toggles." + toggle) ? ChatColor.GREEN : ChatColor.RED) + toggle);
            }
            return true;
        }
        if (args.length == 1)
        {
            for (String toggle : Pylon.plugin.getConfig().getConfigurationSection("toggles").getKeys(false))
            {
                if (args[0].equalsIgnoreCase(toggle))
                {
                    Pylon.plugin.getConfig().set("toggles." + toggle, !Pylon.plugin.getConfig().getBoolean("toggles." + toggle));
                    Pylon.plugin.saveConfig();
                    sender.sendMessage(ChatColor.GOLD + "Toggled " + toggle + (Pylon.plugin.getConfig().getBoolean("toggles." + toggle) ? " on." : " off."));
                    Pylon_ToggleableEventsListener.checkTime();
                    return true;
                }
            }
            sender.sendMessage(ChatColor.AQUA + "Possible toggles:");
            for (String toggle : Pylon.plugin.getConfig().getConfigurationSection("toggles").getKeys(false))
            {
                sender.sendMessage(" - " + (Pylon.plugin.getConfig().getBoolean("toggles." + toggle) ? ChatColor.GREEN : ChatColor.RED) + toggle);
            }
            return true;
        }
        return false;
    }

}
