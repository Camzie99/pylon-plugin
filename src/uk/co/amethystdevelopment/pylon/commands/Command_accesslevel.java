package uk.co.amethystdevelopment.pylon.commands;

import uk.co.amethystdevelopment.pylon.Pylon_Rank;
import uk.co.amethystdevelopment.pylon.Pylon_RestManager;
import uk.co.amethystdevelopment.pylon.Pylon;
import static uk.co.amethystdevelopment.pylon.Pylon.config;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@CommandParameters(name = "accesslevel", usage = "/accesslevel [level]", description = "Change server access level.", aliases = "al, alevel, accessl, ld, ldown, lockdown, lockd", rank = Pylon_Rank.Rank.ADMIN)
public class Command_accesslevel
{
    
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(args.length < 1)
        {
            return false;
        }
        int level;
        try
        {
            level = Integer.parseInt(args[0]);
        }
        catch(Exception ex)
        {
            level = Pylon_Rank.nameToRank(StringUtils.join(ArrayUtils.subarray(args, 0, args.length), " ")).level;
        }
        if(Pylon_Rank.getRank(sender).level < level)
        {
            sender.sendMessage(ChatColor.RED + "You can only set the access level to your rank or lower.");
            return true;
        }
        Pylon.plugin.getConfig().set("general.accessLevel", level);
        Pylon.plugin.saveConfig();
        for(Player player : Bukkit.getOnlinePlayers())
        {
            if(Pylon_Rank.getRank(player).level < level)
            {
                player.kickPlayer("Server has been put into lockdown mode.");
            }
        }
        String message = sender.getName() + " has locked the server down to level " + level + ".";
        if(level == 0)
        {
            message = sender.getName() + " has reopened the server to all players.";
        }
        Pylon_RestManager.sendMessage(config.getInt("rest.lockdownid"), message);
        Bukkit.broadcastMessage(ChatColor.AQUA + sender.getName() + " - Locking server down to clearance level " + level + " (" + Pylon_Rank.levelToRank(level).name + ").");
        return true;
    }
    
}
