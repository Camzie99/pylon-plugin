package uk.co.amethystdevelopment.pylon.commands;

import uk.co.amethystdevelopment.pylon.Pylon_Commons;
import uk.co.amethystdevelopment.pylon.Pylon_Rank;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

@CommandParameters(name = "overlord", usage = "/overlord", description = "Toggle Camzie99's Overlord Mode.", rank = Pylon_Rank.Rank.OVERLORD)
public class Command_overlord
{
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        Pylon_Commons.camOverlordMode = !Pylon_Commons.camOverlordMode;
        sender.sendMessage(ChatColor.AQUA + "Overlord mode turned " + (Pylon_Commons.camOverlordMode ? (ChatColor.GREEN + "on") : (ChatColor.RED + "off")) + ChatColor.AQUA  + ".");
        return true;
    }
}
