package uk.co.amethystdevelopment.pylon.commands;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.StevenLawson.BukkitTelnet.BukkitTelnet;
import me.StevenLawson.BukkitTelnet.session.ClientSession;
import net.camtech.camutils.CUtils_Methods;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import uk.co.amethystdevelopment.pylon.Pylon_DatabaseInterface;
import uk.co.amethystdevelopment.pylon.Pylon_Rank;
import uk.co.amethystdevelopment.pylon.Pylon_Rank.Rank;

@CommandParameters(name = "csay", usage = "/csay [level] [message]", description = "Send a message to a specific chat.", rank = Pylon_Rank.Rank.ADMIN)
public class Command_csay
{

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(args.length < 2)
        {
            return false;
        }
        int level = 0;
        try
        {
            level = Integer.parseInt(args[0]);
        }
        catch(Exception ex)
        {
            return false;
        }
        String replaceAll = CUtils_Methods.colour(StringUtils.join(ArrayUtils.subarray(args, 1, args.length), " ").replaceAll("&-", ""));
        if(Pylon_Rank.getRank(sender).level >= level)
        {
            try
            {
                Connection c = Pylon_DatabaseInterface.getConnection();
                PreparedStatement statement = c.prepareStatement("INSERT INTO LEVELCHATS (PLAYER, LEVEL, MESSAGE) VALUES (?, ?, ?)");
                statement.setString(1, sender.getName());
                statement.setInt(2, level);
                statement.setString(3, replaceAll);
                statement.executeUpdate();
                for(Player player2 : Bukkit.getOnlinePlayers())
                {
                    if(Pylon_Rank.getRank(player2).level >= level)
                    {
                        ChatColor colour = ChatColor.WHITE;
                        String levelString = "" + level;
                        switch(levelString)
                        {
                            case "0":
                                colour = ChatColor.WHITE;
                                break;
                            case "1":
                                colour = ChatColor.YELLOW;
                                break;
                            case "2":
                                colour = ChatColor.AQUA;
                                break;
                            case "3":
                                colour = ChatColor.LIGHT_PURPLE;
                                break;
                            case "4":
                                colour = ChatColor.GOLD;
                                break;
                            case "5":
                                colour = ChatColor.GREEN;
                                break;
                            case "6":
                                colour = ChatColor.DARK_AQUA;
                                break;
                            case "7":
                                colour = ChatColor.BLUE;
                                break;
                            default:
                                break;
                        }
                        player2.sendMessage(colour + "[" + Pylon_Rank.levelToRank(level).name + " Chat] " + sender.getName() + ": " + replaceAll);
                    }
                }
                ChatColor colour = ChatColor.WHITE;
                String levelString = "" + level;
                switch(levelString)
                {
                    case "1":
                        colour = ChatColor.YELLOW;
                        break;
                    case "2":
                        colour = ChatColor.AQUA;
                        break;
                    case "3":
                        colour = ChatColor.LIGHT_PURPLE;
                        break;
                    case "4":
                        colour = ChatColor.GOLD;
                        break;
                    case "5":
                        colour = ChatColor.GREEN;
                        break;
                    case "6":
                        colour = ChatColor.DARK_PURPLE;
                        break;
                    case "7":
                        colour = ChatColor.DARK_RED;
                        break;
                    default:
                        break;
                }
                if(level <= 3)
                {
                    Bukkit.getConsoleSender().sendMessage(colour + "[" + Pylon_Rank.levelToRank(level).name + " Chat] " + sender.getName() + ": " + replaceAll);
                }
                if(Bukkit.getPluginManager().getPlugin("BukkitTelnet").isEnabled())
                {
                    for(ClientSession session : BukkitTelnet.getClientSessions())
                    {
                        String name = session.getCommandSender().getName().replaceAll("[^A-Za-z0-9]", "");
                        Rank rank = Pylon_Rank.getFromUsername(name);
                        if(rank.level >= level)
                        {
                            session.getCommandSender().sendMessage(colour + "[" + Pylon_Rank.levelToRank(level).name + " Chat] " + sender.getName() + ": " + replaceAll);
                        }
                    }
                }
            }
            catch(SQLException ex)
            {
                Logger.getLogger(Command_csay.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else
        {
            sender.sendMessage(ChatColor.RED + "You do not have permission to use this chat level.");
        }
        return true;
    }
}
