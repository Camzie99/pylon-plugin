package uk.co.amethystdevelopment.pylon.commands;

import uk.co.amethystdevelopment.pylon.Pylon_Commons;
import uk.co.amethystdevelopment.pylon.Pylon_DatabaseInterface;
import uk.co.amethystdevelopment.pylon.Pylon_Rank.Rank;
import uk.co.amethystdevelopment.pylon.Pylon;
import org.apache.commons.lang.StringEscapeUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command_banhammer extends Pylon_Command
{

    public Command_banhammer()
    {
        super("banhammer", "/banhammer", "Unleash the banhammer...", Rank.SPECIALIST);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        try
        {
            if(!(sender instanceof Player))
            {
                sender.sendMessage(ChatColor.RED + "Only in-game players can use this command.");
                return true;
            }
            Player player = (Player) sender;
            if(Pylon_DatabaseInterface.hasBanHammer(player.getUniqueId().toString()))
            {
                player.getInventory().remove(Pylon_Commons.getBanHammer());
                Pylon_DatabaseInterface.updateInTable("UUID", player.getUniqueId().toString(), false, "BANHAMMER", "PLAYERS");
                Bukkit.broadcastMessage(ChatColor.AQUA + player.getName() + " has placed the BanHammer back into its sheath");
                return true;
            }
            player.getInventory().addItem(Pylon_Commons.getBanHammer());
            player.getWorld().strikeLightning(player.getLocation());
            Bukkit.broadcastMessage(ChatColor.RED + player.getName() + " has unleashed the BanHammer!");
            Pylon_DatabaseInterface.updateInTable("UUID", player.getUniqueId().toString(), true, "BANHAMMER", "PLAYERS");
            return true;
        }
        catch(Exception ex)
        {
            Pylon.plugin.handleException(ex);
        }
        return true;
    }
}
