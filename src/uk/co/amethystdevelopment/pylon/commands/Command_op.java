package uk.co.amethystdevelopment.pylon.commands;

import uk.co.amethystdevelopment.pylon.Pylon_Commons;
import uk.co.amethystdevelopment.pylon.Pylon_Rank;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@CommandParameters(name = "op", usage = "/op [player]", description = "Op a player.", rank = Pylon_Rank.Rank.OP)
public class Command_op
{
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(args.length != 1)
        {
            return false;
        }
        Player player = Pylon_Rank.getPlayer(args[0]);
        if(player == null)
        {
            sender.sendMessage(ChatColor.RED + args[0] + " is offline.");
            return true;
        }
        player.setOp(true);
        Pylon_Commons.adminAction(sender.getName(), "Opping " + player.getName(), false);
        return true;
    }
}
