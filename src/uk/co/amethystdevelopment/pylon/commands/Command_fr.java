package uk.co.amethystdevelopment.pylon.commands;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Arrays;
import uk.co.amethystdevelopment.pylon.Pylon_Commons;
import uk.co.amethystdevelopment.pylon.Pylon_DatabaseInterface;
import uk.co.amethystdevelopment.pylon.Pylon_Rank;
import uk.co.amethystdevelopment.pylon.Pylon_Rank.Rank;
import uk.co.amethystdevelopment.pylon.Pylon;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command_fr extends Pylon_Command
{

    public Command_fr()
    {
        super("fr", "/fr <player>", "Freeze a player.", Arrays.asList("freeze", "halt"), Rank.ADMIN);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        try
        {
            if(args.length == 0)
            {
                Pylon_Commons.adminAction(sender.getName(), "Toggling freeze over all players on the server.", true);
                Pylon_Commons.globalFreeze = !Pylon_Commons.globalFreeze;
                Connection c = Pylon_DatabaseInterface.getConnection();
                for(Player player : Bukkit.getOnlinePlayers())
                {
                    if(Pylon_Rank.isAdmin(player))
                    {
                        continue;
                    }
                    PreparedStatement statement = c.prepareStatement("UPDATE PLAYERS SET FROZEN = ? WHERE UUID = ?");
                    statement.setBoolean(1, Pylon_Commons.globalFreeze);
                    statement.setString(2, player.getUniqueId().toString());
                    statement.executeUpdate();
                    player.sendMessage((Pylon_DatabaseInterface.getBooleanFromTable("UUID", player.getUniqueId().toString(), "FROZEN", "PLAYERS") ? (ChatColor.RED + "You are now frozen.") : (ChatColor.AQUA + "You are now unfrozen.")));
                }
                c.commit();
                return true;
            }
            if(args.length == 1)
            {
                Player player = Pylon_Rank.getPlayer(args[0]);
                if(player == null)
                {
                    sender.sendMessage("Player is not online.");
                    return true;
                }
                if(Pylon_Rank.isEqualOrHigher(Pylon_Rank.getRank(player), Pylon_Rank.getRank(sender)))
                {
                    sender.sendMessage(ChatColor.RED + "You can only freeze someone who is a lower clearance level than yourself.");
                    return true;
                }
                Pylon_Commons.adminAction(sender.getName(), "Toggling freeze over " + player.getName() + ".", true);
                Pylon_DatabaseInterface.updateInTable("UUID", player.getUniqueId().toString(), !(Pylon_DatabaseInterface.getBooleanFromTable("UUID", player.getUniqueId().toString(), "FROZEN", "PLAYERS")), "FROZEN", "PLAYERS");
                player.sendMessage((Pylon_DatabaseInterface.getBooleanFromTable("UUID", player.getUniqueId().toString(), "FROZEN", "PLAYERS") ? (ChatColor.RED + "You are now frozen.") : (ChatColor.AQUA + "You are now unfrozen.")));
                return true;
            }
        }
        catch(Exception ex)
        {
            Pylon.plugin.handleException(ex);
        }
        return false;
    }

}
