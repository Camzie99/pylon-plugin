/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.co.amethystdevelopment.pylon.commands;

import uk.co.amethystdevelopment.pylon.Pylon_Rank;
import uk.co.amethystdevelopment.pylon.Pylon_Rank.Rank;
import uk.co.amethystdevelopment.pylon.Pylon;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;

@CommandParameters(name="givepermission", description="Give a user a custom permission flag!", usage="/givepermission [player] [permission]",rank=Rank.OWNER)
public class Command_givepermission
{
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(args.length != 2)
        {
            return false;
        }
        Player player = Pylon_Rank.getPlayer(args[0]);
        if(player == null)
        {
            sender.sendMessage(ChatColor.RED + "The player could not be found.");
            return true;
        }
        PermissionAttachment attach = player.addAttachment(Pylon.plugin);
        attach.setPermission(args[1], true);
        return true;
    }
}
