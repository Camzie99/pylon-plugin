package uk.co.amethystdevelopment.pylon.commands;

import me.libraryaddict.disguise.DisguiseAPI;
import uk.co.amethystdevelopment.pylon.Pylon_Rank;
import static uk.co.amethystdevelopment.pylon.Pylon_Rank.Rank.ADMIN;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@CommandParameters(name="uall",description="Undisguise all players.",usage="/uall",rank=ADMIN)
public class Command_uall
{
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(Bukkit.getPluginManager().getPlugin("LibsDisguises") == null)
        {
            sender.sendMessage(ChatColor.RED + "LibsDigsuises could not be found.");
            return true;
        }
        for(Player player : Bukkit.getOnlinePlayers())
        {
            if(Pylon_Rank.getRank(sender).level > Pylon_Rank.getRank(player).level)
            {
                DisguiseAPI.undisguiseToAll(player);
            }
        }
        return true;
    }
}
