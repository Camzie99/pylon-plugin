package uk.co.amethystdevelopment.pylon.commands;

import net.camtech.camutils.CUtils_Methods;
import uk.co.amethystdevelopment.pylon.Pylon_DatabaseInterface;
import uk.co.amethystdevelopment.pylon.Pylon_Rank;
import uk.co.amethystdevelopment.pylon.Pylon_Rank.Rank;
import uk.co.amethystdevelopment.pylon.Pylon;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@CommandParameters(name = "setlogin", description = "Change yours or somebody else's login message. (Player must be online at the present time)", usage = "/setlogin [player] [message]", rank = Rank.EXECUTIVE)
public class Command_setlogin
{

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(args.length < 2)
        {
            return false;
        }
        Player player = Pylon_Rank.getPlayer(args[0]);
        if(player == null)
        {
            sender.sendMessage(ChatColor.RED + "The player you listed: " + args[0] + " is not online...");
            return true;
        }
        try
        {
            String message = StringUtils.join(ArrayUtils.subarray(args, 1, args.length), " ");
            Pylon_DatabaseInterface.updateInTable("UUID", player.getUniqueId().toString(), message, "LOGIN", "PLAYERS");
            sender.sendMessage(ChatColor.GREEN + "Set " + player.getName() + "'s login message to \"" + CUtils_Methods.colour(message) + "\".");
        }
        catch(Exception ex)
        {
            Pylon.plugin.handleException(ex);
        }
        return true;
    }
}
