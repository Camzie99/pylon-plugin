package uk.co.amethystdevelopment.pylon.commands;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import uk.co.amethystdevelopment.pylon.Pylon_Rank.Rank;

@CommandParameters(name="emergencyremoval", description="Remove a block from a world!", usage="/emergencyremoval <x> <y> <z> <world>", rank=Rank.ADMIN)
public class Command_emergencyremoval
{
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(args.length != 4)
        {
            return false;
        }
        try
        {
            int x = Integer.parseInt(args[0]);
            int y = Integer.parseInt(args[1]);
            int z = Integer.parseInt(args[2]);
            World world = Bukkit.getWorld(args[3]);
            world.getBlockAt(x, y, z).setType(Material.AIR);
            sender.sendMessage("Block removed from location.");
        }
        catch(NumberFormatException ex)
        {
            return false;
        }
        return true;
    }
}
