package uk.co.amethystdevelopment.pylon.commands;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import uk.co.amethystdevelopment.pylon.Pylon_DatabaseInterface;
import uk.co.amethystdevelopment.pylon.Pylon_Rank.Rank;

@CommandParameters(name = "setmotd", description = "Set the custom MOTD for a player!", usage = "/setmotd [player] [message]", rank = Rank.SYSTEM)
public class Command_setmotd
{
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(args.length < 2)
        {
            return false;
        }
        String player = args[0];
        OfflinePlayer p = Bukkit.getOfflinePlayer(player);
        try
        {
            if(p != null && Pylon_DatabaseInterface.playerExists(p.getUniqueId().toString()))            
            {
                Pylon_DatabaseInterface.updateInTable("UUID", p.getUniqueId().toString(), StringUtils.join(args, " ", 1, args.length), "MOTD", "PLAYERS");
                return true;
            }
            sender.sendMessage("That player could not be found.");
        }
        catch(SQLException ex)
        {
            Logger.getLogger(Command_setmotd.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
}
