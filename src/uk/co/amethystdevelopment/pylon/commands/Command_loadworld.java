package uk.co.amethystdevelopment.pylon.commands;

import uk.co.amethystdevelopment.pylon.Pylon_Commons;
import uk.co.amethystdevelopment.pylon.Pylon_DatabaseInterface;
import static uk.co.amethystdevelopment.pylon.Pylon_Rank.Rank.SPECIALIST;
import uk.co.amethystdevelopment.pylon.Pylon;
import uk.co.amethystdevelopment.pylon.worlds.Pylon_WorldManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

@CommandParameters(name = "loadworld", usage = "/loadworld [world]", description = "Load an unloaded world.", rank = SPECIALIST)
public class Command_loadworld
{

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(args.length != 1)
        {
            return false;
        }
        for(World world : Bukkit.getWorlds())
        {
            if(world.getName().equals(args[0]))
            {
                sender.sendMessage(ChatColor.RED + "The world you are trying to load, is already loaded!");
                return true;
            }
        }
        try
        {
            if(Pylon_DatabaseInterface.getFromTable("NAME", args[0], "NAME", "WORLDS") == null)
            {
                sender.sendMessage(ChatColor.RED + "The world you are trying to load, does not exist or is not a custom AFM world.");
                return true;
            }
            Pylon_WorldManager.loadWorld(args[0]);
            World world = Bukkit.getWorld(args[0]);
            Pylon_Commons.adminAction(sender.getName(), "loading the world: \"" + world.getName() + "\" into memory.", false);
        }
        catch(Exception ex)
        {
            Pylon.plugin.handleException(ex);
        }
        return true;
    }
}
