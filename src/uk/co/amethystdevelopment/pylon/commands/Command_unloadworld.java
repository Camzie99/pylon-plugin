package uk.co.amethystdevelopment.pylon.commands;

import net.camtech.camutils.CUtils_Methods;
import uk.co.amethystdevelopment.pylon.Pylon_Commons;
import uk.co.amethystdevelopment.pylon.Pylon_DatabaseInterface;
import static uk.co.amethystdevelopment.pylon.Pylon_Rank.Rank.SPECIALIST;
import uk.co.amethystdevelopment.pylon.Pylon;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

@CommandParameters(name = "unloadworld", usage = "/unloadworld [world]", description = "Unload an loaded world.", rank = SPECIALIST)
public class Command_unloadworld
{

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(args.length != 1)
        {
            return false;
        }
        boolean loaded = false;
        for(World world : Bukkit.getWorlds())
        {
            if(world.getName().equals(args[0]))
            {
                loaded = true;
            }
        }
        if(!loaded)
        {
            sender.sendMessage(ChatColor.RED + "The world you are trying to unload, is not loaded.");
            return true;
        }
        try
        {
            if(Pylon_DatabaseInterface.getFromTable("NAME", args[0], "NAME", "WORLDS") != null)
            {
                sender.sendMessage(ChatColor.RED + "The world you are trying to load, does not exist or is not a custom AFM world.");
                return true;
            }
            CUtils_Methods.unloadWorld(Bukkit.getWorld(args[0]));
            Pylon_Commons.adminAction(sender.getName(), "unloading the world: \"" + args[0] + "\" from memory.", true);
        }
        catch(Exception ex)
        {
            Pylon.plugin.handleException(ex);
        }
        return true;
    }
}
