package uk.co.amethystdevelopment.pylon.commands;

import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import uk.co.amethystdevelopment.pylon.Pylon_Rank;
import uk.co.amethystdevelopment.pylon.Pylon_Rank.Rank;

public class Command_wildcard extends Pylon_Command
{
    public Command_wildcard()
    {
        super("wildcard", "/wildcard [command]", "Run a command once for every player on the server (? gets replaced with their name).", Rank.SUPER);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if (args.length == 0)
        {
            return false;
        }

        List<String> blocked = Arrays.asList("doom", "ban", "wildcard", "smite", "forcechat", "fchat", "fc");

        String baseCommand = StringUtils.join(args, " ");

        for (String block : blocked)
        {
            if (baseCommand.toLowerCase().contains(block) && !Pylon_Rank.isSpecialist(sender))
            {
                sender.sendMessage(ChatColor.RED + String.format("You cannot use %s in a WildCard!", block));
                return true;
            }
        }

        for (Player player : Bukkit.getServer().getOnlinePlayers())
        {
            String out_command = baseCommand.replaceAll("\\x3f", player.getName());
            sender.sendMessage("Running Command: " + out_command);
            Bukkit.getServer().dispatchCommand(sender, out_command);
        }

        return true;
    }

}
