package uk.co.amethystdevelopment.pylon.commands;

import java.util.Arrays;
import uk.co.amethystdevelopment.pylon.Pylon_BoardManager;
import uk.co.amethystdevelopment.pylon.Pylon_DatabaseInterface;
import uk.co.amethystdevelopment.pylon.Pylon;
import org.apache.commons.lang.StringEscapeUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command_djump extends Pylon_Command
{

    public Command_djump()
    {
        super("djump", "/djump", "Toggle your double jumping ability.", Arrays.asList("doublejump"));
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(!(sender instanceof Player))
        {
            sender.sendMessage("This can only be used in game.");
            return true;
        }
        Player player = (Player) sender;
        sender.sendMessage(ChatColor.GREEN + "Toggled double jump mode.");
        try
        {
            Pylon_DatabaseInterface.updateInTable("UUID", player.getUniqueId().toString(), !(Pylon_DatabaseInterface.getBooleanFromTable("UUID", player.getUniqueId().toString(), "DOUBLEJUMP", "PLAYERS")), "DOUBLEJUMP", "PLAYERS");
        }
        catch(Exception ex)
        {
            Pylon.plugin.handleException(ex);
        }
        Pylon_BoardManager.updateStats(player);
        return true;
    }

}
