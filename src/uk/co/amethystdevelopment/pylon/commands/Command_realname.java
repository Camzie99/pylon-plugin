package uk.co.amethystdevelopment.pylon.commands;

import net.camtech.camutils.CUtils_Methods;
import uk.co.amethystdevelopment.pylon.Pylon_DatabaseInterface;
import uk.co.amethystdevelopment.pylon.Pylon;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

@CommandParameters(name = "realname", description = "Find the real name of a player from their nickname.", usage = "/realname [player]")
public class Command_realname
{

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(args.length != 1)
        {
            return false;
        }
        try
        {
            for(Object obj : Pylon_DatabaseInterface.getAsArrayList("UUID", null, "UUID", "PLAYERS"))
            {
                String uuid = (String) obj;
                String nick = (String) Pylon_DatabaseInterface.getFromTable("UUID", uuid, "NICK", "PLAYERS");
                String name = (String) Pylon_DatabaseInterface.getFromTable("UUID", uuid, "NAME", "PLAYERS");
                String onoroff = ((Bukkit.getPlayer(name) != null) ? (ChatColor.GREEN + "online") : (ChatColor.RED + "offline"));
                if(nick.toLowerCase().contains(args[0].toLowerCase()))
                {
                    sender.sendMessage(CUtils_Methods.colour(nick + ChatColor.WHITE + " is " + name + " and is " + onoroff + ChatColor.WHITE + "."));
                }
            }
        }
        catch(Exception ex)
        {
            Pylon.plugin.handleException(ex);
        }
        return true;
    }
}
