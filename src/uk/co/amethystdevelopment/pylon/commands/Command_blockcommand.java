package uk.co.amethystdevelopment.pylon.commands;

import java.util.Arrays;
import uk.co.amethystdevelopment.pylon.Pylon_DatabaseInterface;
import uk.co.amethystdevelopment.pylon.Pylon_Rank;
import uk.co.amethystdevelopment.pylon.Pylon_Rank.Rank;
import uk.co.amethystdevelopment.pylon.Pylon;
import org.apache.commons.lang.StringEscapeUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command_blockcommand extends Pylon_Command
{

    public Command_blockcommand()
    {
        super("blockcommand", "/blockcommand [player]", "Block a player's commands.", Arrays.asList("blockcmd"), Rank.ADMIN);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(args.length != 1)
        {
            return false;
        }
        Player player = Pylon_Rank.getPlayer(args[0]);
        if(player == null)
        {
            sender.sendMessage("Player is not online.");
            return true;
        }
        if(Pylon_Rank.isEqualOrHigher(Pylon_Rank.getRank(player), Pylon_Rank.getRank(sender)))
        {
            sender.sendMessage("You can only block the commands of a player with lower clearance than yourself.");
            return true;
        }
        Bukkit.broadcastMessage(ChatColor.AQUA + sender.getName() + " - toggling command blockage for " + player.getName() + ".");
        try
        {
            Pylon_DatabaseInterface.updateInTable("UUID", player.getUniqueId().toString(), !(Pylon_DatabaseInterface.getBooleanFromTable("UUID", player.getUniqueId().toString(), "CMDBLOCK", "PLAYERS")), "CMDBLOCK", "PLAYERS");
        }
        catch(Exception ex)
        {
            Pylon.plugin.handleException(ex);
        }
        return true;
    }

}
