package uk.co.amethystdevelopment.pylon.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import uk.co.amethystdevelopment.pylon.Pylon;
import uk.co.amethystdevelopment.pylon.Pylon_DatabaseInterface;
import uk.co.amethystdevelopment.pylon.Pylon_Rank;

@CommandParameters(name = "pylon", usage = "/pylon <reload>", description = "Check info about the plugin or reload the configuration file.")
public class Command_pylon
{

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {

        if(args.length > 0)
        {
            if(args[0].equalsIgnoreCase("reload"))
            {
                if(!Pylon_Rank.isAdmin(sender))
                {
                    sender.sendMessage(ChatColor.RED + "Only admins can reload the Pylon config.");
                    return true;
                }
                Bukkit.broadcastMessage(ChatColor.GOLD + "Pylon data is reloading, temporary lag may occur whilst caches are rebuilt.");
                Pylon_Rank.ranks.clear();
                Pylon_Rank.nicks.clear();
                Pylon_Rank.tags.clear();
                Pylon.plugin.reloadConfig();
                Pylon_DatabaseInterface.closeConnection(Pylon_DatabaseInterface.getConnection());
            }
            return true;
        }
        sender.sendMessage(ChatColor.GREEN + "Welcome to the Pylon Plugin!");
        sender.sendMessage(ChatColor.DARK_AQUA + "This plugin aims to solve many of the issues that I felt plague the TFM.");
        sender.sendMessage(ChatColor.DARK_AQUA + "This is the new form of what was previously known as the FreedomOpMod: Remastered and AmethystFreedomMod.");
        sender.sendMessage(ChatColor.BLUE + "Pylon - The monumental gateway to our temple.");
        return true;
    }

}
