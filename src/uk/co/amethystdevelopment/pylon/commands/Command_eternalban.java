package uk.co.amethystdevelopment.pylon.commands;

import uk.co.amethystdevelopment.pylon.Pylon_Bans;
import uk.co.amethystdevelopment.pylon.Pylon_DatabaseInterface;
import uk.co.amethystdevelopment.pylon.Pylon_Rank.Rank;
import uk.co.amethystdevelopment.pylon.Pylon;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

@CommandParameters(name = "eternalban", description = "Toggles the eternalban flag on a user's ban entry.", usage = "/eternalban <player>", rank = Rank.SYSTEM, aliases = "permban, eternban")
public class Command_eternalban
{

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(args.length != 1)
        {
            return false;
        }
        if(!Pylon_Bans.isBanned(args[0]))
        {
            sender.sendMessage(ChatColor.RED + "The player: " + args[0] + " is not banned.");
            return true;
        }
        try
        {
            Pylon_DatabaseInterface.updateInTable("NAME", args[0], !(Pylon_DatabaseInterface.getBooleanFromTable("NAME", args[0], "PERM", "NAME_BANS")), "PERM", "NAME_BANS");
            Pylon_DatabaseInterface.updateInTable("IP", Pylon_DatabaseInterface.getIpFromName(args[0]), !(Pylon_DatabaseInterface.getBooleanFromTable("IP", Pylon_DatabaseInterface.getIpFromName(args[0]), "PERM", "IP_BANS")), "PERM", "IP_BANS");
            Pylon_DatabaseInterface.updateInTable("UUID", Pylon_DatabaseInterface.getUuidFromName(args[0]), !(Pylon_DatabaseInterface.getBooleanFromTable("UUID", Pylon_DatabaseInterface.getUuidFromName(args[0]), "PERM", "UUID_BANS")), "PERM", "UUID_BANS");
            String message = (Pylon_DatabaseInterface.getBooleanFromTable("NAME", args[0], "PERM", "NAME_BANS") ? ChatColor.GREEN + "Toggled eternal ban on." : ChatColor.RED + "Toggled eternal ban off.");
            sender.sendMessage(message);
        }
        catch(Exception ex)
        {
            Pylon.plugin.handleException(ex);
        }
        return true;
    }
}
