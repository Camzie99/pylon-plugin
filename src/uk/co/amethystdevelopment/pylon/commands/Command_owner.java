package uk.co.amethystdevelopment.pylon.commands;

import uk.co.amethystdevelopment.pylon.Pylon_Commons;
import uk.co.amethystdevelopment.pylon.Pylon_Rank;
import uk.co.amethystdevelopment.pylon.Pylon;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@CommandParameters(name="owner", usage="/owner <code>", description="Verify yourself as the owner, check the console for more details. (Disables after first usage!)")
public class Command_owner
{
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(args.length != 1)
        {
            return false;
        }
        if(!(sender instanceof Player))
        {
            sender.sendMessage(ChatColor.RED + "This command must be sent from in-game.");
            return true;
        }
        if(Pylon_Commons.verifyCode == null)
        {
            sender.sendMessage(ChatColor.RED + "An owner has already been set.");
            return true;
        }
        if(args[0].equals(Pylon_Commons.verifyCode))
        {
            Pylon_Commons.adminAction(sender.getName(), "Verifying myself as the Owner!", false);
            Pylon_Rank.setRank((Player) sender, Pylon_Rank.Rank.OWNER, null);
            Pylon_Commons.verifyCode = null;
            Pylon.plugin.getConfig().set("general.owner", true);
            Pylon.plugin.saveConfig();
            return true;
        }
        else
        {
            sender.sendMessage(ChatColor.RED + "This is the incorrect verification code!");
            return true;
        }
    }
}
