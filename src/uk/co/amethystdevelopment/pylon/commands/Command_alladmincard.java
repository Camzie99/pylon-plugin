package uk.co.amethystdevelopment.pylon.commands;

import uk.co.amethystdevelopment.pylon.Pylon_DatabaseInterface;
import uk.co.amethystdevelopment.pylon.Pylon_Rank.Rank;
import uk.co.amethystdevelopment.pylon.Pylon;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@CommandParameters(name = "alladmincard", usage = "/alladmincard [command]", description = "Run a command once for every admin on the admin list (? gets replaced with their name).", rank = Rank.SYSTEM)
public class Command_alladmincard
{

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        try
        {
            if(args.length == 0)
            {
                return false;
            }
            String baseCommand = StringUtils.join(args, " ");

            for(Player player : Bukkit.getOnlinePlayers())
            {
                for(Object result : Pylon_DatabaseInterface.getAsArrayList("UUID", null, "UUID", "PLAYERS"))
                {
                    String uuid = (String) result;
                    if(!((String) Pylon_DatabaseInterface.getFromTable("UUID", uuid, "RANK", "PLAYERS")).equalsIgnoreCase("Op"))
                    {
                        String out_command = baseCommand.replaceAll("\\x3f", (String) Pylon_DatabaseInterface.getFromTable("UUID", uuid, "NAME", "PLAYERS"));
                        sender.sendMessage("Running Command: " + out_command);
                        Bukkit.dispatchCommand(sender, out_command);
                    }
                }
            }
        }
        catch(Exception ex)
        {
            Pylon.plugin.handleException(ex);
        }
        return true;
    }
}
