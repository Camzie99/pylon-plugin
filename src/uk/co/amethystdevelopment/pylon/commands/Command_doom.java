package uk.co.amethystdevelopment.pylon.commands;

import uk.co.amethystdevelopment.pylon.Pylon_Bans;
import uk.co.amethystdevelopment.pylon.Pylon_Rank;
import uk.co.amethystdevelopment.pylon.Pylon_Rank.Rank;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command_doom extends Pylon_Command
{
    public Command_doom()
    {
        super("doom", "/doom [player] [reason]", "Ban someone and remove them from the admin list.", Rank.SUPER);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if (args.length < 2)
        {
            return false;
        }
        Player player = Pylon_Rank.getPlayer(args[0]);
        if (player == null)
        {
            sender.sendMessage("Player is not online.");
            return true;
        }
        if (Pylon_Rank.isEqualOrHigher(Pylon_Rank.getRank(player), Pylon_Rank.getRank(sender)))
        {
            sender.sendMessage("You can only doom someone of a lower rank than yourself.");
            return true;
        }
        String reason = StringUtils.join(ArrayUtils.subarray(args, 1, args.length), " ");
        Pylon_Bans.addBan(player, reason, sender.getName());
        Pylon_Rank.setRank(player, Rank.OP, sender);
        Bukkit.broadcastMessage(ChatColor.RED + sender.getName() + " - Dooming " + player.getName() + " to a never-ending oblivion in the deepest burning pits of Hell.");
        return true;
    }

}
