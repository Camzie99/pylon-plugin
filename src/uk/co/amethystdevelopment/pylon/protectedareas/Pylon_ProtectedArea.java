package uk.co.amethystdevelopment.pylon.protectedareas;

import com.google.gson.Gson;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import uk.co.amethystdevelopment.pylon.Pylon_DatabaseInterface;
import uk.co.amethystdevelopment.pylon.Pylon_Rank;
import uk.co.amethystdevelopment.pylon.Pylon_Rank.Rank;
import uk.co.amethystdevelopment.pylon.Pylon;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class Pylon_ProtectedArea
{

    private final String owner;
    private final String name;
    private final ArrayList<String> allowed;
    private final Rank rank;
    private final Location loc;
    private final int range;

    public Pylon_ProtectedArea(String owner, String name, ArrayList<String> allowed, Rank rank, Location loc, int range)
    {
        this.owner = owner;
        this.name = name;
        this.allowed = allowed;
        this.rank = rank;
        this.loc = loc;
        this.range = range;
    }

    public String getName()
    {
        return this.name;
    }

    public String getOwner()
    {
        return this.owner;
    }

    public Rank getRank()
    {
        return this.rank;
    }

    public ArrayList<String> getAllowed()
    {
        return this.allowed;
    }

    public Location getLocation()
    {
        return this.loc;
    }

    public int getRange()
    {
        return this.range;
    }

    public boolean canAccess(Player player)
    {
        if(isOwner(player))
        {
            return true;
        }
        if(allowed.contains(player.getName()))
        {
            return true;
        }
        return Pylon_Rank.getRank(player).level > rank.level;
    }

    public boolean addPlayer(Player sender, Player player)
    {
        if(!isOwner(sender) && getRank().level > Pylon_Rank.getRank(sender).level)
        {
            return false;
        }
        if(this.allowed.contains(player.getName()) || this.isOwner(player))
        {
            return false;
        }
        try
        {
            this.allowed.add(player.getName());
            Connection c = Pylon_DatabaseInterface.getConnection();
            PreparedStatement statement = c.prepareStatement("UPDATE OR IGNORE AREAS SET ALLOWED = ? WHERE NAME = ?");
            statement.setString(1, (new Gson()).toJson(this.allowed));
            statement.setString(2, this.name);
            statement.executeUpdate();
            c.commit();
            player.sendMessage(ChatColor.GREEN + "You have been added to the protected area " + this.name + " by " + sender.getName() + ".");
            return true;
        }
        catch(Exception ex)
        {
            Pylon.plugin.handleException(ex);
        }
        return false;
    }

    public boolean removePlayer(Player sender, Player player)
    {
        try
        {
            if((!isOwner(sender) || !this.allowed.contains(player.getName())) && getRank().level > Pylon_Rank.getRank(sender).level)
            {
                return false;
            }
            if(this.allowed.contains(player.getName()))
            {
                this.allowed.remove(player.getName());
                Connection c = Pylon_DatabaseInterface.getConnection();
                PreparedStatement statement = c.prepareStatement("UPDATE OR IGNORE AREAS SET ALLOWED = ? WHERE NAME = ?");
                statement.setString(1, (new Gson()).toJson(this.allowed));
                statement.setString(2, this.name);
                statement.executeUpdate();
                c.commit();
                return true;
            }
        }
        catch(Exception ex)
        {
            Pylon.plugin.handleException(ex);
        }
        return false;
    }

    public boolean isOwner(Player player)
    {
        return (this.owner == null ? player.getName() == null : this.owner.equals(player.getName()));
    }

    public boolean isInRange(Location location)
    {
        if(this.loc.getWorld() == location.getWorld())
        {
            return this.loc.distance(location) < range;
        }
        return false;
    }
}
