package uk.co.amethystdevelopment.pylon;

import java.util.Set;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.command.CommandSender;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.permissions.PermissionAttachmentInfo;
import org.bukkit.plugin.Plugin;
import uk.co.amethystdevelopment.pylon.Pylon_Rank.Rank;

public class Pylon_UtilitySender implements CommandSender
{
    private final String name;
    private final Rank rank;
    private final String code;
    
    public Pylon_UtilitySender(String name, String code, Rank rank)
    {
        this.name = name;
        this.code = code;
        this.rank = rank;
    }
    
    public String getCode()
    {
        return this.code;
    }
    
    public Rank getRank()
    {
        return this.rank;
    }
    
    @Override
    public void sendMessage(String message)
    {
        Bukkit.getServer().getConsoleSender().sendMessage(message);
    }

    @Override
    public void sendMessage(String[] messages)
    {
        Bukkit.getServer().getConsoleSender().sendMessage(messages);
    }

    @Override
    public Server getServer()
    {
        return null;
    }

    @Override
    public String getName()
    {
        return this.name;
    }

    @Override
    public boolean isPermissionSet(String name)
    {
        return true;
    }

    @Override
    public boolean isPermissionSet(Permission perm)
    {
        return true;
    }

    @Override
    public boolean hasPermission(String name)
    {
        return true;
    }

    @Override
    public boolean hasPermission(Permission perm)
    {
        return true;
    }

    @Override
    public PermissionAttachment addAttachment(Plugin plugin, String name, boolean value)
    {
        return null;
    }

    @Override
    public PermissionAttachment addAttachment(Plugin plugin)
    {
        return null;
    }

    @Override
    public PermissionAttachment addAttachment(Plugin plugin, String name, boolean value, int ticks)
    {
        return null;
    }

    @Override
    public PermissionAttachment addAttachment(Plugin plugin, int ticks)
    {
        return null;
    }

    @Override
    public void removeAttachment(PermissionAttachment attachment)
    {

    }

    @Override
    public void recalculatePermissions()
    {

    }

    @Override
    public Set<PermissionAttachmentInfo> getEffectivePermissions()
    {
        return null;
    }

    @Override
    public boolean isOp()
    {
        return true;
    }

    @Override
    public void setOp(boolean value)
    {

    }
}
