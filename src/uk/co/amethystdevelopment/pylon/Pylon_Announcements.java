package uk.co.amethystdevelopment.pylon;

import java.sql.ResultSet;
import net.camtech.camutils.CUtils_Methods;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

public class Pylon_Announcements
{

    public static void setup()
    {
        System.out.println("Announcements Loading.");
        try
        {
            ResultSet set = Pylon_DatabaseInterface.getAllResults("", null, "ANNOUNCEMENTS");
            while(set.next())
            {
                Object message = set.getObject("MESSAGE");
                Object interval = set.getObject("INTERVAL");
                if(message instanceof String && interval instanceof Integer)
                {
                    announce((String) message, (Integer) interval);
                }
            }
        }
        catch(Exception ex)
        {
            Pylon.plugin.handleException(ex);
        }
    }

    private static void announce(final String message, final long interval)
    {
        new BukkitRunnable()
        {
            @Override
            public void run()
            {
                Bukkit.broadcastMessage(CUtils_Methods.colour(message));
            }
        }.runTaskTimerAsynchronously(Pylon.plugin, 0, interval * 20);
        
    }
}
