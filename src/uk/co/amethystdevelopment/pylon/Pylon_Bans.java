package uk.co.amethystdevelopment.pylon;

import java.sql.Connection;
import java.sql.PreparedStatement;
import net.camtech.camutils.CUtils_Methods;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;
import static uk.co.amethystdevelopment.pylon.Pylon.config;

public class Pylon_Bans
{

    public static void addBan(final Player player, final String reason, String banner)
    {
        addBan(player, reason, banner, false);
    }

    public static void addBan(String name, String reason, String banner)
    {
        addBan(name, reason, banner, false);
    }

    public static void addBan(final Player player, final String reason, String banner, boolean post)
    {
        player.setGameMode(GameMode.SURVIVAL);
        player.getInventory().clear();
        player.setVelocity(player.getVelocity().add(new Vector(0, 3, 0)));
        new BukkitRunnable()
        {
            @Override
            public void run()
            {
                player.kickPlayer(reason);
                player.getWorld().createExplosion(player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getZ(), 10f, false, false);
                player.getWorld().strikeLightning(player.getLocation());
            }
        }.runTaskLater(Pylon.plugin, 20L * 3L);
        player.getWorld().createExplosion(player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getZ(), 10f, false, false);
        player.getWorld().strikeLightning(player.getLocation());
        player.setHealth(0d);
        addBan(player.getName(), reason, banner, post);
    }

    public static void addBan(String name, String reason, String banner, boolean post)
    {
        if(post)
        {
            String message = name + " has been banned by " + banner + " with the reason: " + reason.split(" — ")[0] + ".";
            Pylon_RestManager.sendMessage(config.getInt("rest.banid"), message);
        }

        Connection c = Pylon_DatabaseInterface.getConnection();
        try
        {
            PreparedStatement statement = c.prepareStatement("REPLACE INTO NAME_BANS (NAME, REASON, PERM) VALUES (?, ?, ?)");
            statement.setString(1, name);
            statement.setString(2, reason);
            statement.setBoolean(3, false);
            statement.executeUpdate();
            if(Pylon_DatabaseInterface.getIpFromName(name) != null)
            {
                statement = c.prepareStatement("REPLACE INTO IP_BANS (IP, REASON, PERM) VALUES (?, ?, ?)");
                statement.setString(1, Pylon_DatabaseInterface.getIpFromName(name));
                statement.setString(2, reason);
                statement.setBoolean(3, false);
                statement.executeUpdate();
            }
            if(Pylon_DatabaseInterface.getUuidFromName(name) != null)
            {
                statement = c.prepareStatement("REPLACE INTO UUID_BANS (UUID, REASON, PERM) VALUES (?, ?, ?)");
                statement.setString(1, Pylon_DatabaseInterface.getUuidFromName(name));
                statement.setString(2, reason);
                statement.setBoolean(3, false);
                statement.executeUpdate();
                c.commit();
            }
        }
        catch(Exception ex)
        {
            Pylon.plugin.handleException(ex);
        }
    }

    public static void unBan(Player player)
    {
        unBan(player.getName());
    }

    public static void unBan(String name)
    {
        Connection c = Pylon_DatabaseInterface.getConnection();
        try
        {
            PreparedStatement statement = c.prepareStatement("DELETE FROM NAME_BANS WHERE NAME = ? AND PERM = 0");
            statement.setString(1, name);
            statement.executeUpdate();
            c.commit();
            statement = c.prepareStatement("DELETE FROM IP_BANS WHERE IP = ? AND PERM = 0");
            statement.setString(1, Pylon_DatabaseInterface.getIpFromName(name));
            statement.executeUpdate();
            c.commit();
            statement = c.prepareStatement("DELETE FROM UUID_BANS WHERE UUID = ? AND PERM = 0");
            statement.setString(1, Pylon_DatabaseInterface.getUuidFromName(name));
            statement.executeUpdate();
            c.commit();
        }
        catch(Exception ex)
        {
            Pylon.plugin.handleException(ex);
        }
    }

    public static boolean isBanned(Player player)
    {
        return isBanned(player.getName(), player.getAddress().getHostName());
    }

    public static boolean isBanned(String name)
    {
        try
        {
            return isBanned(name, Pylon_DatabaseInterface.getIpFromName(name));
        }
        catch(Exception ex)
        {
            Pylon.plugin.handleException(ex);
        }
        return false;
    }

    public static boolean isBanned(String name, String ip)
    {
        try
        {
            if(Pylon_DatabaseInterface.getFromTable("NAME", name, "NAME", "NAME_BANS") != null)
            {
                return true;
            }

            if(Pylon_DatabaseInterface.getFromTable("IP", ip, "IP", "IP_BANS") != null)
            {
                return true;
            }

            if(Pylon_DatabaseInterface.getFromTable("UUID", Pylon_DatabaseInterface.getUuidFromName(name), "UUID", "UUID_BANS") != null)
            {
                return true;
            }
        }
        catch(Exception ex)
        {
            Pylon.plugin.handleException(ex);
        }
        return false;
    }

    public static String getReason(String name, String ip)
    {
        if(!isBanned(name, ip))
        {
            return "Player is not banned.";
        }
        if("70.189.160.159".equals(ip))
        {
            return "Massive imposter, fuck off, you're hardcoded to permban you twat.";
        }
        try
        {
            if(Pylon_DatabaseInterface.getFromTable("NAME", name, "NAME", "NAME_BANS") != null)
            {
                return (String) Pylon_DatabaseInterface.getFromTable("NAME", name, "REASON", "NAME_BANS");
            }
            if(Pylon_DatabaseInterface.getFromTable("IP", ip, "IP", "IP_BANS") != null)
            {
                return (String) Pylon_DatabaseInterface.getFromTable("IP", ip, "REASON", "IP_BANS");
            }
            if(Pylon_DatabaseInterface.getFromTable("UUID", Bukkit.getOfflinePlayer(name).getUniqueId().toString(), "UUID", "UUID_BANS") != null)
            {
                return (String) Pylon_DatabaseInterface.getFromTable("UUID", Bukkit.getOfflinePlayer(name).getUniqueId().toString(), "REASON", "UUID_BANS");
            }
        }
        catch(Exception ex)
        {
            Pylon.plugin.handleException(ex);
            return "An exception occurred...";
        }
        if(CUtils_Methods.containsSimilar(name, "PvP"))
        {
            return "Stupid idiot who serial griefs always using the same kind of name, welcome to hardcoded permban.";
        }
        return "Player is not banned.";
    }

    public static String getReason(String name)
    {
        try
        {
            return getReason(name, Pylon_DatabaseInterface.getIpFromName(name));
        }
        catch(Exception ex)
        {
            Pylon.plugin.handleException(ex);
            return "An exception occurred";
        }
    }
}
