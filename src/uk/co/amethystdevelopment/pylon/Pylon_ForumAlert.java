package uk.co.amethystdevelopment.pylon;

public class Pylon_ForumAlert
{
    private final long postId, threadId;
    private final String poster, posted, title;
    
    public Pylon_ForumAlert(long postId, long threadId, String poster, String posted, String title)
    {
        this.postId = postId;
        this.threadId = threadId;
        this.poster = poster;
        this.posted = posted;
        this.title = title;
    }

    public long getPostId()
    {
        return postId;
    }

    public long getThreadId()
    {
        return threadId;
    }

    public String getPoster()
    {
        return poster;
    }

    public String getPosted()
    {
        return posted;
    }

    public String getTitle()
    {
        return title;
    }
}
